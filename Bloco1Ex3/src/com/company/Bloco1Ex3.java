package com.company;

public class Bloco1Ex3 {
    public static void main(String[] args) {

        double raio;
        double altura;

        System.out.println(quantidadeLitros(4,4));
    }

    public static double quantidadeLitros(double raioBase, double alturaCilindro)
    {
        double vol;
        double litros;

        vol = Math.PI *Math.pow(raioBase,2)*alturaCilindro;
        litros=vol*1000;

        return litros;

    }
}