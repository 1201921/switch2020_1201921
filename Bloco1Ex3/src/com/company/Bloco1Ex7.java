package com.company;

public class Bloco1Ex7 {
    public static void main(String[] args) {
        double distManel=42.195;
        double tempoManel=4+(2/60)+((10/60)/60);    // 04h02m10s
        double tempoZe=1+(5/60);    //01h05m00s

        System.out.println(distanciaZe(42.195,4+(2/60)+((10/60)/60),1+(5/60))); 	//=10.54875
    }

    public static double distanciaZe (double distManel, double tempoManel, double tempoZe ){
        double velMediaManel = distManel / tempoManel; //velocidade média = deslocamento/tempo
        double distZe=velMediaManel*tempoZe; //descolamento = velocidade média * tempo
        return distZe;
    }
}
