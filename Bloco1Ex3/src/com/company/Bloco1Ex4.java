package com.company;

public class Bloco1Ex4 { //Exercicio4 - distancia da trovoada
    public static void main(String[] args) {        //vel. luz 300 000 000 m/s (1 079 252 859 km/h)
                                                    //tempo=Diferença entre ver relampago e ouvir trovão
		double velocidadeSom;                    //vel. som 1224 km/h (340m/s) => m/s= km/h / 3.6; km/h = m/s * 3.6
        double tempoSeg=14.7;                         //dist=vSom*(tempo entre relampago e trovão em segundos)

        System.out.println(distanciaTrovoada(340, tempoSeg));

    }

    public static double distanciaTrovoada (double velocidadeSom, double tempoSeg) {
        double dist = velocidadeSom*tempoSeg;
        return dist;
    }
}
