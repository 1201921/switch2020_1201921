package com.company;

public class Bloco1Ex8 { //Distancia entre operários
    public static void main(String[] args) {
        double compCabo1=60, compCabo2=40, cosY=60;
        System.out.println(distanciaOperarios(60,40,60));
    }

    public static double distanciaOperarios (double compCabo1, double compCabo2, double cosY){
        cosY=Math.cos(Math.toRadians(cosY));
        double distOperarios = Math.sqrt((Math.pow(compCabo1,2) + Math.pow(compCabo2,2) - 2*(compCabo1*compCabo2)*cosY));
        //distOperariosE2=compCabo1E2+compCabo1E2 - 2(compCabo2*compCabo1)*cosY
        return distOperarios;
    }
}
