package com.company;
/*Exercicio5 - Altura do Edificio pela gravidade
		Double alt;
		Double aceleracao = 9.8, tempo = 5.0;
		int velocidadeInicial=0;
		alt=(velocidadeInicial*tempo)+((aceleracao*(tempo*tempo))/2);
		System.out.println(alt);
		*/
public class Bloco1Ex5 {
    public static void main(String[] args) {
        double velocidadeInicial=0.0;
        double aceleracao=9.8;
        double tempo=2;

        System.out.println(alturaPredio(0,9.8,2));
    }

    public static double alturaPredio(double velocidadeInicial, double aceleracao, double tempo) {
        double alt = (velocidadeInicial*tempo) + ((aceleracao*(Math.pow(tempo, 2))/2));
        return alt;
    }
}

