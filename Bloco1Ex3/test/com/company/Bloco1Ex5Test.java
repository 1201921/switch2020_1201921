package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex5Test {

    @Test
    void alturaPredio() {
        double velocidadeInicial=0.0;
        double aceleracao=9.8;
        double tempo=2;

        double expected = 19.6;
        double result = Bloco1Ex5.alturaPredio(velocidadeInicial,aceleracao,tempo);
        assertEquals(expected, result, 0.01);
    }
}