package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex7Test {

    @Test
    void distanciaZe() {
        double distManel=42.195;
        double tempoManel=4+(2/60)+((10/60)/60);
        double tempoZe=1+(5/60);

        double expected = 10.54875;
        double result = Bloco1Ex7.distanciaZe(distManel, tempoManel, tempoZe);
        assertEquals(expected, result, 0.01);
    }
}