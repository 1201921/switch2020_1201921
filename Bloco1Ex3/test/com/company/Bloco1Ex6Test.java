package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex6Test {

    @Test
    void alturaEdificio() {
        double sombraPredio = 40.0;
        double sombraPessoa = 4.0;
        double altPessoa = 2.0;

        double expected = 20.0;
        double result = Bloco1Ex6.alturaEdificio(sombraPredio, sombraPessoa, altPessoa);
        assertEquals(expected, result, 0.01);
    }
}