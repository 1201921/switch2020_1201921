package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex4Test {

    @Test
    void distanciaTrovoada() {
        double velocidadeSom = 340;
        double tempoSeg = 10;

        double expected = 3400;
        double result = Bloco1Ex4.distanciaTrovoada (velocidadeSom,tempoSeg);
        assertEquals(expected, result, 0.001);
    }
}