package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex3Test {

    @Test
    void quantidadeLitros() {
        double raio=4;
        double altura=4;

        double expected = 201061.9298;
        double result = Bloco1Ex3.quantidadeLitros(raio, altura);
        assertEquals(expected, result, 0.01);

    }
}