package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco1Ex8Test {
    @Test
    void distanciaOperarios() {
        double compCabo1=60;
        double compCabo2=40;
        double cosY=60;

        double expected = 52.91502622129181;
        double result = Bloco1Ex8.distanciaOperarios(compCabo1, compCabo2,cosY);
        assertEquals(expected, result, 0.01);
    }
}