import java.util.Arrays;
import java.util.Scanner;
public class Bloco4Ex6 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        int i=0, n;

        int [] vetor = new int[100]; //número por excesso
        int num=0;
        do{
            System.out.println("Insira um número inteiro para o vetor.\nPara parar inserir '0'");
            num=ler.nextInt();
            vetor[i]=num;
            i++;
        } while(num!=0);

        System.out.println("Qual o valor de N?");
        n=ler.nextInt();

        System.out.println();
        System.out.println(Arrays.toString(obterVetorComOsPrimeirosNElementos(vetor, n)));
    }

    public static int [] obterVetorComOsPrimeirosNElementos (int [] vetor, int n){
        int [] novoVetor = new int [n];
        for (int i = 0; i<n; i++){
            novoVetor[i] = vetor [i];
        }
        return novoVetor;
    }
}
