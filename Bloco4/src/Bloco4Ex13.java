import java.util.Scanner;

public class Bloco4Ex13 {
    public static void main(String[] args) { //verificar se a matriz é quadrada » nº colunas = nº linhas
        int a [][]; //= {{1,2,3},{1,2,3}};       // considerando que a matriz não tem valores vazios nem nulos

        //System.out.println("Colunas: " + contarNumeroColunas(a));
        //System.out.println("Linhas: " + contarNumeroLinhas(a));
        //System.out.println("Matriz quadrada? " + verificarMatrizQuadrada(a));
    }

    public static int contarNumeroLinhas (int a [][]){
        int numeroLinhas=0;

        for (int i = 0; i < a.length; i++) {
            numeroLinhas++;
        }

        return numeroLinhas;
    }

    public static int contarNumeroColunas (int a[][]){
        int numeroColunas=0;
        int numeroLinhas=contarNumeroLinhas(a);

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                numeroColunas++;
            }
        }
        numeroColunas=numeroColunas/numeroLinhas;
        return numeroColunas;
    }

    public static boolean verificarMatrizQuadrada (int [][] a){
        boolean matrizQuadrada = false;

        if (contarNumeroColunas(a) == contarNumeroLinhas(a)){
            matrizQuadrada=true;
        }
        return matrizQuadrada;
    }
}
