import java.util.Arrays;

public class Bloco4Ex10 {
    public static void main(String[] args) {
        int [] vetor;



    }

    //a)
    public static int getMenorValor (int[]vetor){
        int menor = vetor[0];
        for (int i = 0; i< vetor.length; i++){
            if(menor > vetor[i]){
                menor=vetor[i];
            }
        }
        return menor;
    }

    //b
    public static int getMaiorValor (int[]vetor){
        int maior = vetor[0];
        for (int i = 0; i< vetor.length; i++){
            if(maior < vetor[i]){
                maior=vetor[i];
            }
        }
        return maior;
    }

    //c)
    public static double getMediaVetor (int []vetor){
        int qtdElementos = 0, soma = 0;
        double media;

        for (int i = 0; i< vetor.length; i++){
            soma = soma + vetor[i];
            qtdElementos++;
        }
        media = (double) soma/qtdElementos;
        return media;
    }

    //d)
    public static int getProdutoElementos (int [] vetor){
        int produto = 1;

        for(int i = 0; i<vetor.length; i++){
            produto *= vetor[i];
        }
        return produto;
    }

    //e)
    public static int [] conjuntoElementosNaoRepetidos (int [] vetor){
        int [] conj = new int [vetor.length];
        int i, j, k=0,count=0, count2=0;

        for (i=0; i<vetor.length; i++){
            count = 0;

            for (j=0; j<vetor.length; j++){
                if (vetor[i] == vetor [j]) count++;
            }
            if( count==1){
                conj [k] = vetor [i];
                k++;
                count2++;
            }
        }
        int [] novoConj = new int [count2];
        //cópia do vetor original que tem 6 colunas para ficar apenas com o tamanho igual ao nº elementos repetidos
        for (i = 0; i<count2; i++){
            novoConj[i] = conj[i];
        }
        return novoConj;
    }

    //f)
    public static int [] getVetorInverso (int [] vetor){
        int [] vetorInverso = new int [vetor.length];
        for(int i = 0, f = vetor.length-1; i < vetor.length; ++i, f--){
                vetorInverso [f] = vetor [i];
            }
        return vetorInverso;
    }

    //g)
    public static int[] getElementosPrimos (int [] vetor){ //{5, 8, 4, 12, 11, 27}
        // se o vetor for nulo ou vazio retorna nulo
        if (vetor == null || vetor.length<1)
            return null;
        int [] VetorOrdenado = ordenarMenorAoMaior(vetor);
        int [] vetorPrimos = new int [vetor.length];
        int count = 0;
        boolean isPrime;

        for (int i = 0; i<vetor.length; i++){ //corre os valores do array
            isPrime = true;
            if(vetor[i] == 0 || vetor[i] == 1) {
                isPrime = false;
            } else{
                for (int j = 2; j<vetor[i] && isPrime == true; j++){ //ver se o nº do array é divisivel por algum nº de ]1 - numero[
                    if (vetor[i] % j == 0)
                    isPrime=false;
                }
            }
            if (isPrime==true){
                    vetorPrimos[count] = vetor [i];
                    count++;
            }
        }

        int [] novoVetorPrimos = new int [count];
        for (int i = 0; i<count; i++) {
            novoVetorPrimos[i] = vetorPrimos[i];
        }

        int [] vetorPrimosFinal = removeNumerosRepetidosArray(novoVetorPrimos);

        return vetorPrimosFinal;
    }

    public static int [] removeNumerosRepetidosArray (int [] vetor){
        int [] vetorOrdemMenorParaMaior=ordenarMenorAoMaior(vetor);
        int [] semNumerosRepetidos = new int [vetor.length];
        int repetidos, m=0, j=0;

        for (int i = 0; i < vetor.length; i++) {
            repetidos = 0;
            for (j = 0; j < vetor.length; j++) {
                if (vetorOrdemMenorParaMaior[i] == vetor[j]) {
                    repetidos++;
                }
            }
            if (repetidos == 1) {
                semNumerosRepetidos[m] = vetorOrdemMenorParaMaior[i];
                m++;
            }
            if (repetidos > 1){
                semNumerosRepetidos[m] = vetorOrdemMenorParaMaior[i];
                m++;
                i=i-1+repetidos;
            }
        }

        int [] novoVetor = new int [m];
        for (int i = 0; i < m; i++) {
            novoVetor[i]=semNumerosRepetidos[i];

        }
        return novoVetor;
    }

    public static int[]ordenarMenorAoMaior (int [] vetor){
        int [] novoVetor = new int [vetor.length];
        int temp=0;

        for (int i = 0; i < vetor.length; i++) {
            for (int j = i+1; j < vetor.length; j++) {
                if (vetor[i]>vetor[j]){
                    temp=vetor[i];
                    vetor[i]=vetor[j];
                    vetor[j]=temp;
                }

            }

        }
            return vetor;
    }
}
