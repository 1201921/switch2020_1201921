import java.util.Scanner;

public class Bloco4Ex7 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int inicioIntervalo, fimIntervalo, multiplo;
        System.out.println("Indique o limite inferior do intervalo:");
        inicioIntervalo= ler.nextInt();
        System.out.println("Indique o limite superior do intervalo:");
        fimIntervalo= ler.nextInt();
        System.out.println("Quer procurar os múltiplos de que número?");
        multiplo=ler.nextInt();

        System.out.println();
        System.out.println(obterMultiplosDeUmNumero(inicioIntervalo,fimIntervalo,multiplo));

    }

    public static String obterMultiplosDeUmNumero (int inicioIntervalo, int fimIntervalo, int multiplo){
        String multiplos="";
        for(int i = inicioIntervalo; i<=fimIntervalo;i++){
            if(i%multiplo==0){
                multiplos = multiplos + i + " ";
            }
        }
        return "Os múltiplos de " + multiplo + " são: " + multiplos;
    }
}
