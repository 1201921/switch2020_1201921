import java.util.Scanner;

public class Bloco4Ex9 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        int num=0;
        System.out.println("Introduza um nº inteiro");
        num = ler.nextInt();

        System.out.println(verificarCapicua(num));

    }

    public static boolean verificarCapicua (int num){
        boolean capicua = true;
        int inverso = 0;

        for (int i = num; i!=0; i=i/10){
            inverso = inverso * 10;
            inverso = inverso + i%10;
        }

        if (inverso!=num){
            return capicua = false;
        }
        return capicua;
    }
}
