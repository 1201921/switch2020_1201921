public class Bloco4Ex14 {
    public static void main(String[] args) {
        int a [][]; //= {{1,2},{1,2}};

        //System.out.println(verificarMatrizQuadrada(a));

    }

    public static boolean verificarMatrizRetangular (int [][] a){
        boolean matrizRetangular = false;

        if (Bloco4Ex13.contarNumeroColunas(a) < Bloco4Ex13.contarNumeroLinhas(a) || Bloco4Ex13.contarNumeroColunas(a) > Bloco4Ex13.contarNumeroLinhas(a)){
            matrizRetangular=true;
        }
        return matrizRetangular;
    }
}
