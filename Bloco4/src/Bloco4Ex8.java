import java.util.Arrays;
import java.util.Scanner;

public class Bloco4Ex8 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        int inicioIntervalo, fimIntervalo, multiplo [] = new int [10];

        System.out.println("Indique o limite inferior do intervalo:");
        inicioIntervalo= ler.nextInt();

        System.out.println("Indique o limite superior do intervalo:");
        fimIntervalo= ler.nextInt();

        System.out.println("Quer procurar os múltiplos de que número?");
        multiplo[0]=ler.nextInt();
        int count=1;

        for (int i = 1; i< multiplo.length; i++){
            System.out.println("Quer procurar os múltiplos de mais algum número? Se não introduza '0'");
            multiplo[i] = ler.nextInt();
            if (multiplo[i]==0) break;
            count++;
        }

        int [] numeros = new int [count]; //cópia do vetor original que tem 10 colunas para ficar apenas com o nº de colunas igual ao nº de numeros que o utilizador introduziu
        for (int i = 0; i<count; i++){
            numeros[i] = multiplo[i];
        }


    }

    public static String obterMultiplosDeNumerosDados (int inicioIntervalo, int fimIntervalo, int [] numeros){
        int [] multiplos;
        int count=0;
        String multiplosComuns = "";

        for (int i=inicioIntervalo; i<=fimIntervalo; i++){ //correr o intervalo dado
            count=0;

            for (int j = 0; j<numeros.length; j++){
                if (i % numeros[j] == 0){
                    count++;
                }
                if (count==numeros.length){
                    multiplosComuns = multiplosComuns + i + " ";
                }
            }
        }
        return multiplosComuns;
    }
}
