import java.util.Arrays;
import java.util.Scanner;
public class Bloco4Ex1 {

    public static void main(String[] args) {
        Scanner ler = new Scanner((System.in));
        System.out.println( "Insira um número inteiro positivo");
        double num = ler.nextDouble();

        while (num<0) {
            System.out.println("Introduza um nº inteiro positivo");
            num = ler.nextDouble();
        }

        int qtdAlgarismosDeUmNum = obterNumeroDigitosDeUmNum(num);
        int [] vetor = obterVetorComAlgarismoDeUmNum(num, qtdAlgarismosDeUmNum);
        int somaAlgarismosVetor = obterSomaAlgarismosVetor(vetor);
        int [] vetorAlgarismosPares = obterVetorComAlgarismosPares(vetor);
        int [] vetorAlgarismosImpares = obterVetorComAlgarismosImpares(vetor);

        System.out.println();
        System.out.println("O numero inserido tem " + obterNumeroDigitosDeUmNum(num) + " algarismos.");
        System.out.println();
        System.out.println("Os algarismos do número indicado formam o vetor: " + Arrays.toString(obterVetorComAlgarismoDeUmNum(num, qtdAlgarismosDeUmNum)));
        System.out.println();
        System.out.println("A soma de todos os algarismos do número é = " + somaAlgarismosVetor);
        System.out.println();
        System.out.println("Os algarismos pares do vetor formam o novo vetor: " + Arrays.toString(obterVetorComAlgarismosPares (vetor)));
        System.out.println();
        System.out.println("Os algarismos impares do vetor formam o novo vetor: " + Arrays.toString(obterVetorComAlgarismosImpares (vetor)));
        System.out.println();
        System.out.println(obterSomaElementosParesDoVetor (vetorAlgarismosPares));
        System.out.println();
        System.out.println(obterSomaElementosImparesDoVetor (vetorAlgarismosImpares));

    }

    /**
     *
     * @param num - numero inserido pelo utilizador (inteiro positivo)
     * @return - Retorna a quantidade de algarismos do numero dado
     */
    public static int obterNumeroDigitosDeUmNum(double num){
        int a = (int) num; //cast para tornar um possível double inserido pelo utilizador em inteiro

        int qtd=0;
        for (int i = 0; a > 0; i++){
            qtd++;
            a = a/10;
        }
        return qtd;
    }

    /**
     *
     * @param num - numero inserido pelo utilizador (inteiro positivo)
     * @return - retorna um vetor formado por cada um dos algarismos de um numero dado
     */
    public static int [] obterVetorComAlgarismoDeUmNum (double num, int qtdAlgarismosDeUmNum){
        int a = (int) num; //cast para tornar um possível double inserido pelo utilizador em inteiro
        int [] vetor = new int [qtdAlgarismosDeUmNum];

        for (int i = 0; a>0; i++){ //Acha os elementos do vetor, do ultimo algarismo até ao 1º
            vetor[i] = a%10;
            a=a/10;
        }

        for (int i = 0, j= vetor.length-1; i<j; i++, j--){   //Copiar vetor para a ordem correta
            int temporaria = vetor[i];
            vetor[i]=vetor[j];
            vetor[j]=temporaria;
        }

        return vetor;
    }

    /**
     *
     * @param vetor - vetor (original) formado pelos algarismos do numero dado pelo utilizador
     * @return - retorna a soma dos algarismos de um vetor
     */
    public static int obterSomaAlgarismosVetor (int [] vetor){
        int soma = 0;
        for (int i = 0; i<vetor.length; i++){
            soma = soma + vetor[i];
        }
        return soma;
    }

    /**
     *
     * @param vetor (original)
     * @return - retorna um vetor formado pelos elementos par do vetor original
     */
    public static int [] obterVetorComAlgarismosPares (int [] vetor){
        int numAlgarismoPar = 0;

        for (int i = 0; i<vetor.length; i++){
            if (vetor[i]%2 == 0){
               numAlgarismoPar++;
            }
        }
        int [] vetorPares = new int [numAlgarismoPar];

        for (int i = 0, j=0; i<vetor.length;i++) { // 1, 2, 3, 4
            if (vetor [i] % 2 == 0){
                vetorPares[j] = vetor[i];
                j++;
            }
        }
        return vetorPares;
    }

    /**
     *
     * @param vetor (original)
     * @return - retorna um vetor formado pelos elementos impar do vetor original
     */
    public static int [] obterVetorComAlgarismosImpares (int [] vetor){
        int numAlgarismoImpar = 0;

        for (int i = 0; i<vetor.length; i++){
            if (vetor[i]%2 != 0){
                numAlgarismoImpar++;
            }
        }
        int [] vetorImpares = new int [numAlgarismoImpar];

        for (int i = 0, j=0; i<vetor.length;i++) { // 1, 2, 3, 4
            if (vetor [i] % 2 != 0){
                vetorImpares[j] = vetor[i];
                j++;
            }
        }
        return vetorImpares;

    }

    public static String obterSomaElementosParesDoVetor (int [] vetorAlgarismosPares) { // o retorno não é exatamente o pretendido (devolve um "+" no final que nao deveria existir)
        int resultadoSoma = 0;
        String somatório = "";

        for (int i = 0; i < vetorAlgarismosPares.length; i++) {
            somatório =  somatório + vetorAlgarismosPares[i] + "+";
            resultadoSoma = resultadoSoma + vetorAlgarismosPares[i];
        }
        String retorno = resultadoSoma + "(" + somatório + ")";
        return retorno;
    }

    public static String obterSomaElementosImparesDoVetor (int [] vetorAlgarismosImpares) {  // o retorno não é exatamente o pretendido (devolve um "+" no final que nao deveria existir)
        int resultadoSoma = 0;
        String somatório = "";

        for (int i = 0; i < vetorAlgarismosImpares.length; i++) {
            somatório =  somatório + vetorAlgarismosImpares[i] + "+";
            resultadoSoma = resultadoSoma + vetorAlgarismosImpares[i];
        }

        String retorno = resultadoSoma + "(" + somatório + ")";
        return retorno;
    }
}
