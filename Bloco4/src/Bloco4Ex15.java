import java.util.Arrays;

public class Bloco4Ex15 {
    public static void main(String[] args) {
        int matriz [][];


    }

    //a)
    public static int getMaiorElemento (int [][] matriz){
        int maior = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (maior < matriz[i][j]) {
                    maior = matriz[i][j];
                }
            }
        }
        return maior;
    }

    //b)
    public static int getMenorElemento (int [][] matriz) {
        int menor = matriz [0][0];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (menor > matriz[i][j]) {
                    menor = matriz[i][j];
                }
            }
        }
        return menor;
    }

    //c)
    public static double getMediaElementos (int [][] matriz){
        double media = (double) somaElementosMatriz(matriz)/contarElementosMatriz(matriz);

        return media;
    }

    public static int contarElementosMatriz (int [][] matriz){
        int numElementos=0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                numElementos++;
            }
        }

        return numElementos;
    }

    public static int somaElementosMatriz (int [][] matriz){
        int soma = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                soma+= matriz[i][j];
            }
        }

        return soma;
    }

    //d)
    public static long produtoElementosMatriz (int [][] matriz){
        long produto = 1L;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                produto *= matriz[i][j];
            }
        }

        return produto;
    }

    //e)
    public static int [] elementosNaoRepetidos (int [][] matriz){

        int [] naoRepetidos = new int [100];
        int  k=0,count, contarNaoRepetidos=0;

        for (int i=0; i<matriz.length; i++){
            for (int f = 0; f < matriz[i].length; f++) {
            count = 0;
                for (int j=0; j<matriz.length; j++){
                    for (int l = 0; l < matriz[j].length; l++) {
                        if (matriz[i][f] == matriz [j][l]) count++;
                    }
                }
                if( count==1){
                    naoRepetidos [k] = matriz [i][f];
                    k++;
                    contarNaoRepetidos++;
                }
            }
        }
        int [] novoNaoRepetidos = new int [contarNaoRepetidos];

        for (int i = 0; i<contarNaoRepetidos; i++){
            novoNaoRepetidos[i] = naoRepetidos[i];
        }

        return novoNaoRepetidos;
    }

    //f)
    public static int [] elementosPrimos (int [][] matriz){
        int [] primos = new int [100];
        int count = 0;
        boolean isPrime;

        if (matriz == null || matriz.length<1)
            return null;

        for (int i = 0; i<matriz.length; i++){
            for (int j = 0; j < matriz[i].length; j++) { //correr os valores da matriz
                isPrime = true;
                if(matriz[i][j] == 0 || matriz [i][j] == 1) {
                    isPrime = false;
                } else{
                    for (int f = 2; f<matriz[i][j] && isPrime == true; f++){ //ver se o nº da matriz é divisivel por algum nº de ]1 - numero[
                        if (matriz[i][j] % f == 0)
                            isPrime=false;
                    }
                }
                if (isPrime==true){
                    primos[count] = matriz [i][j];
                    count++;
                }
            }
        }

        int [] novoPrimos = new int [count];
        for (int i = 0; i<count; i++) {             //eliminar nºs repetidos do array
            novoPrimos[i] = primos[i];
        }

        int a [] = Bloco4Ex10.removeNumerosRepetidosArray(novoPrimos);
        return a;
    }

    /*public static int [][] removeNumerosDuplicados (int [][] matriz){
        int m=0, n=0, repetições;
        int matrizSemDuplicados[][] = new int [2][6];
        for (int i = 0; i < matriz.length; i++) {
            n=0;
            for (int j = 0; j < matriz[i].length; j++) {
                repetições=0;

                for (int k = i; k < matriz.length; k++) {

                    for (int l = j; l < matriz[k].length && repetições<2; l++) {
                        if(matriz [i][j] == matriz [k][l]){
                            repetições++;
                        }
                    }
                }
                if(repetições==1) {
                    matrizSemDuplicados[m][n] = matriz[i][j];
                    n++;
                }
            }
            m++;
        }
        return matrizSemDuplicados;
    }*/

    //g)
    public static int [] diagonalPrincipalMatriz (int [][] matriz){
        int [] diagonalPrincipal = new int [matriz.length];
        int i=0, j=0;

        do{
            diagonalPrincipal[i]=matriz[i][j];
            i++;
            j++;
        } while (i<matriz.length);

        return diagonalPrincipal;
    }

    //h)
    public static int [] diagonalSecundariaMatriz (int [][] matriz){
        int [] diagonalSecundaria = new int [matriz.length];
        int i=0, j=matriz[i].length-1, k=0;

        do{
            diagonalSecundaria[k]=matriz[i][j];
            k++;
            i++;
            j--;
        } while (i<matriz.length);

        return diagonalSecundaria;
    }

    //i))
    public static boolean verificarMatrizIdentidade (int [][] matriz){ //Matriz identidade tem de ser quadrada
        boolean verificarMatriz = true;
        int [] diagonalPrincipal = diagonalPrincipalMatriz(matriz);

        //verificar se a matriz é quadrada
        if(Bloco4Ex13.verificarMatrizQuadrada(matriz)==false){
            verificarMatriz=false;}

        //garantir que a matriz é apenas composta por 1 e 0
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if(matriz[i][j] < 0 || matriz[i][j] > 1){
                    verificarMatriz=false;}
            }
        }

        //garantir que a diagonal principal só tem 1's
        for (int i = 0; i < diagonalPrincipal.length; i++) {
            if (diagonalPrincipal[i]!=1)
                verificarMatriz=false;
        }

        //garantir que para além da diagonal Principal não existem 1's
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if(i==j){
                    continue;
                }
                if (matriz[i][j] != 0){
                    verificarMatriz=false;
                }
            }
        }

        return verificarMatriz;
    }

    //j)
    public static int [][] getMatrizInversa (int [][] matriz){
        
    }

}
