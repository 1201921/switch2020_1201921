import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex11Test {

    @Test
    void getProdutoEscalarDoisVetores() {
        int [] vetor1 = {2,4};
        int [] vetor2 = {5,3};

        int expected = 22;
        int result = Bloco4Ex11.getProdutoEscalarDoisVetores(vetor1, vetor2);
        assertEquals(expected, result);
    }
}