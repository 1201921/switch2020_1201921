import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex9Test {

    @Test
    void verificarCapicuaTest1() {
        boolean expected = true;
        boolean result = Bloco4Ex9.verificarCapicua(101);
        assertEquals(expected, result);
    }

    @Test
    void verificarCapicuaTest2() {
        boolean expected = false;
        boolean result = Bloco4Ex9.verificarCapicua(100);
        assertEquals(expected, result);
    }
}