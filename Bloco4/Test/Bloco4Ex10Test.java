import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex10Test {

    @Test
    void getMenorValorTest1() {
        int [] vetor = {4, 5, 3, 1, 5};

        int expected = 1;
        int result = Bloco4Ex10.getMenorValor(vetor);
        assertEquals(expected, result);
    }

    @Test
    void getMenorValorTest2() {
        int [] vetor = {4, 0, -3, 1, 5};

        int expected = -3;
        int result = Bloco4Ex10.getMenorValor(vetor);
        assertEquals(expected, result);
    }

    @Test
    void getMaiorValor() {
        int [] vetor = {4, 5, 3, 1, 2};

        int expected = 5;
        int result = Bloco4Ex10.getMaiorValor(vetor);
        assertEquals(expected, result);
    }


    @Test
    void getMediaVetorTest1() {
        int [] vetor = {2, 4, 2, 4, 2, 4};

        double expected = 3.0;
        double result = Bloco4Ex10.getMediaVetor(vetor);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaVetorTest2() {
        int [] vetor = {25, 42, 224, 4, 32, 544};

        double expected = 145.16;
        double result = Bloco4Ex10.getMediaVetor(vetor);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getProdutoElementosTest1() {
        int [] vetor = {2, 4, 2, 4, 2, 4};

        int expected = 512;
        int result = Bloco4Ex10.getProdutoElementos(vetor);
        assertEquals(expected, result);
    }

    @Test
    void getProdutoElementosTest2() {
        int [] vetor = {2, 4, -2, 4, 2, 4};

        int expected = -512;
        int result = Bloco4Ex10.getProdutoElementos(vetor);
        assertEquals(expected, result);
    }

    @Test
    void conjuntoElementosNaoRepetidosTest1() {
        int [] vetor = {1, 1, 2, 3, 3, 4};

        int [] expected = {2, 4};
        int [] result = Bloco4Ex10.conjuntoElementosNaoRepetidos(vetor);
        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoElementosNaoRepetidosTest2() {
        int [] vetor = {4, 1, 4, 3, 5, 6};

        int [] expected = {1, 3, 5, 6};
        int [] result = Bloco4Ex10.conjuntoElementosNaoRepetidos(vetor);
        assertArrayEquals(expected, result);
    }

    @Test
    void getVetorInversoTest1() {
        int [] vetor = {3,55,2};

        int [] expected = {2,55,3};
        int [] result = Bloco4Ex10.getVetorInverso(vetor);
        assertArrayEquals(expected, result);
    }

    @Test
    void getVetorInverso() {
        int [] vetor = {-50, 0, 5, 0};

        int [] expected = {0, 5, 0, -50};
        int [] result = Bloco4Ex10.getVetorInverso(vetor);
        assertArrayEquals(expected, result);
    }

    @Test
    void getElementosPrimos() {
        int [] vetor = {5, 8, 4, 12,7, 7, 11, 27};

        int [] expected = {5,7,11};
        int [] result = Bloco4Ex10.getElementosPrimos(vetor);
        //System.out.println(Arrays.toString(result));
        assertArrayEquals(expected, result);
    }
}