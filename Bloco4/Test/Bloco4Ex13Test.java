import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex13Test {

    @Test
    void contarNumeroColunas() {
        int a [][] = {{1,2},{1,2}};

        int expected = 2;
        int result = Bloco4Ex13.contarNumeroColunas(a);
        assertEquals(expected,result);
    }

    @Test
    void contarNumeroLinhas() {
        int a [][] = {{1,2},{1,2}};

        int expected = 2;
        int result = Bloco4Ex13.contarNumeroLinhas(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarMatrizQuadradaTest1() {
        int a [][] = {{1,2},{1,2}};

        boolean expected = true;
        boolean result = Bloco4Ex13.verificarMatrizQuadrada(a);
        assertEquals(expected,result);
    }

    @Test
    void verificarMatrizQuadradaTest2() {
        int a [][] = {{1,2,3},{1,2,3}};

        boolean expected = false;
        boolean result = Bloco4Ex13.verificarMatrizQuadrada(a);
        assertEquals(expected,result);
    }

    @Test
    void verificarMatrizQuadradaTest3() {
        int a [][] = {{1,2,3},{1,2,3},{1,2,4}};

        boolean expected = true;
        boolean result = Bloco4Ex13.verificarMatrizQuadrada(a);
        assertEquals(expected,result);
    }

    @Test
    void contarNumeroColunasTest2() {
        int a [][] = {{1,2,3},{1,2,3}};

        int expected = 3;
        int result = Bloco4Ex13.contarNumeroColunas(a);
        assertEquals(expected,result);
    }

    @Test
    void contarNumeroLinhasTest2() {
        int a [][] = {{1,2,3},{1,2,3}};

        int expected = 2;
        int result = Bloco4Ex13.contarNumeroLinhas(a);
        assertEquals(expected, result);
    }



}