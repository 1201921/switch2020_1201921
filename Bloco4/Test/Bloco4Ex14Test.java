import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex14Test {

    @Test
    void verificarMatrizRetangularTest1() {
        int a [][] = {{1,2},{1,3}};

        boolean expected = false;
        boolean result = Bloco4Ex14.verificarMatrizRetangular(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarMatrizRetangularTest2() {
        int a [][] = {{4,5,2},{1,3,2},{4,2,1},{2,5,4}};

        boolean expected = true;
        boolean result = Bloco4Ex14.verificarMatrizRetangular(a);
        assertEquals(expected, result);
    }
}