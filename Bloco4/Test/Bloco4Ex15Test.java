import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex15Test {

    @Test
    void getMaiorElemento() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        int expected = 333;
        int result = Bloco4Ex15.getMaiorElemento(matriz);
        assertEquals(expected, result);
    }

    @Test
    void getMenorElemento() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        int expected = 4;
        int result = Bloco4Ex15.getMenorElemento(matriz);
        assertEquals(expected, result);
    }

    @Test
    void getMediaElementos() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        double expected = 65.67;
        double result = Bloco4Ex15.getMediaElementos(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void contarElementosMatriz() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        int expected = 9;
        int result = Bloco4Ex15.contarElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void somaElementosMatriz() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        int expected = 591;
        int result = Bloco4Ex15.somaElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void produtoElementosMatriz() {
        int matriz [][] = {{22, 44, 33, 55,} , {32, 23, 333, 45, 4}};

        long expected = 77508001612800L;
        long result = Bloco4Ex15.produtoElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidosTest1() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        int [] expected = {5,3,6};
        int [] result = Bloco4Ex15.elementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidosTest2() {
        int matriz [][] = {{-1,4,2,6} , {2,3,4,4,4}};

        int [] expected = {-1,6,3};
        int [] result = Bloco4Ex15.elementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosPrimos() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        int [] expected = {2,3,5};
        int [] result = Bloco4Ex15.elementosPrimos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosPrimosTest2() {
        int matriz [][] = {};

        int [] expected = null;
        int [] result = Bloco4Ex15.elementosPrimos(matriz);
        assertArrayEquals(expected,result);

    }

    @Test
    void diagonalPrincipalMatrizTest1() {
        int matriz [][] = {{1,1} , {2,3}};

        int [] expected = {1,3};
        int [] result = Bloco4Ex15.diagonalPrincipalMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalPrincipalMatriz() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        int [] expected = {1,3};
        int [] result = Bloco4Ex15.diagonalPrincipalMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalSecundariaMatrizTest1() {
        int matriz [][] = {{1,1} , {2,3}};

        int [] expected = {1,2};
        int [] result = Bloco4Ex15.diagonalSecundariaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalSecundariaMatrizTest2() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        int [] expected = {5,4};
        int [] result = Bloco4Ex15.diagonalSecundariaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void verificarMatrizIdentidadeMatrizNaoQuadrada() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        boolean expected = false;
        boolean result = Bloco4Ex15.verificarMatrizIdentidade(matriz);
        assertEquals(expected,result);
    }

    @Test
    void verificarMatrizIdentidadeValoresDiferentesZeroOuUm() {
        int matriz [][] = {{1,0,2} , {0,1,0} , {0,0,1}};

        boolean expected = false;
        boolean result = Bloco4Ex15.verificarMatrizIdentidade(matriz);
        assertEquals(expected,result);
    }

    @Test
    void verificarMatrizIdentidade() {
        int matriz [][] = {{1,1,0} , {0,1,0} , {0,0,1}};

        boolean expected = false;
        boolean result = Bloco4Ex15.verificarMatrizIdentidade(matriz);
        assertEquals(expected,result);
    }

    @Test
    void verificarMatrizIdentidadeVerdadeira() {
        int matriz [][] = {{1,0,0} , {0,1,0} , {0,0,1}};

        boolean expected = true;
        boolean result = Bloco4Ex15.verificarMatrizIdentidade(matriz);
        assertEquals(expected,result);
    }

    @Test
    void getMatrizInversaTest1() {
        int matriz [][] = {{1,1,2,2,5} , {2,3,4,4,6}};

        int [][] expected = {{6,4,4,3,2},{5,2,2,1,1}};
        int [][] result = Bloco4Ex15.getMatrizInversa(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrizInversaTest2() {
        int matriz [][] = {{1,1,2,2} , {2,3,4,4,6}};

        int [][] expected = {{6,4,4,3,2},{2,2,1,1}};
        int [][] result = Bloco4Ex15.getMatrizInversa(matriz);
        assertArrayEquals(expected, result);
    }

}