import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex8Test {

    @Test
    void obterMultiplosDeNumerosDadosTest1() {
        int inicio=1;
        int fim=10;
        int [] numeros = {2,4};

        String expected = "4 8 ";
        String result = Bloco4Ex8.obterMultiplosDeNumerosDados(inicio,fim,numeros);
        assertEquals(expected, result);
    }

    @Test
    void obterMultiplosDeNumerosDadosTest2() {
        int inicio=1;
        int fim=10;
        int [] numeros = {2,5};

        String expected = "10 ";
        String result = Bloco4Ex8.obterMultiplosDeNumerosDados(inicio,fim,numeros);
        assertEquals(expected, result);
    }
}