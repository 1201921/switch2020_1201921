import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex12Test {

   @Test
    void verificarNumeroColunasTest1() {
        int[][] a = {{1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}};


        int expected = 4;
        int result = Bloco4Ex12.verificarNumeroColunas(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroColunasTest2() {
        int[][] a = {{1, 2, 3, 4}, {1, 2, 3}, {1, 2, 3, 4}};


        int expected = -1;
        int result = Bloco4Ex12.verificarNumeroColunas(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroColunasTest3() {
        int[][] a = {{1, 2, 3,4}, {1, 2, 3, 4}, {1, 2}};


        int expected = -1;
        int result = Bloco4Ex12.verificarNumeroColunas(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroColunasTest4() {
        int[][] a = {{1, 2}, {1, 2, 3, 4}, {1, 2, 3}};


        int expected = -1;
        int result = Bloco4Ex12.verificarNumeroColunas(a);
        assertEquals(expected, result);
    }

}