import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco4Ex1A5Test {
    @org.junit.jupiter.api.Test

    void obterNumeroDigitosDeUmNumTest1() {
        int expected = 5;
        int result = Bloco4Ex1a5.obterNumeroDigitosDeUmNum(54621);
        assertEquals(expected, result);
    }


    @Test
    void obterNumeroDigitosDeUmNumTest2() {
        int expected = 3;
        int result = Bloco4Ex1a5.obterNumeroDigitosDeUmNum(100);
        assertEquals(expected, result);
    }

    @Test
    void obterNumeroDigitosDeUmNumTest3() {
        int expected = 0;
        int result = Bloco4Ex1a5.obterNumeroDigitosDeUmNum(-1);
        assertEquals(expected, result);
    }

    @Test
    void obterNumeroDigitosDeUmNumTest4() {
        int expected = 3;
        int result = Bloco4Ex1a5.obterNumeroDigitosDeUmNum(784.5);
        assertEquals(expected, result);
    }

    @Test
    void obterVetorComAlgarismoDeUmNum() {
        int [] expected = {1, 2, 3, 4};
        int [] result = Bloco4Ex1a5.obterVetorComAlgarismoDeUmNum(1234,4);
        assertArrayEquals(expected, result);
    }

    @Test
    void obterSomaAlgarismosVetor() {
        int [] vetor = {1, 2, 3, 4};

        int expected = 10;
        int result = Bloco4Ex1a5.obterSomaAlgarismosVetor(vetor);
        assertEquals(expected, result);
    }




}