import java.util.Arrays;

public class Matriz {
    //atributos
    private Vetor[] matriz;

    //construtor

    /**
     * a) Construtor de um vetor vazio
     */
    public Matriz() {
        Vetor[] matriz = new Vetor[0];
        this.matriz = matriz;
    }

    /**
     * b) Contrutor que permite inicializar um array de vetores do tipo Vetor
     *
     * @param vetores
     */
    public Matriz(Vetor[] vetores) { //conjunto de vetores
        checkMatrizNula(vetores);
        Vetor[] novaMatriz = new Vetor[vetores.length];
        for (int i = 0; i < vetores.length; i++) {
            novaMatriz[i] = vetores[i];
        }
        this.matriz = novaMatriz;
    }

    private void checkMatrizNula(Vetor[] matriz) {
        if (matriz == null) {
            throw new IllegalArgumentException("O vetor bidimensional não é válido.");
        }
    }

    /**
     * b) Contrutor que permite inicializar um array de vetores do tipo int []
     *
     * @param vetores
     */
    public Matriz(int[][] vetores) {
        Vetor[] novaMatriz = new Vetor[vetores.length];
        for (int i = 0; i < vetores.length; i++) {
            int[] array = new int[vetores[i].length];
            for (int j = 0; j < vetores[i].length; j++) {
                array[j] = vetores[i][j];
            }
            Vetor novoVetor = new Vetor(array); //converter o int array para a classe do ex
            novaMatriz[i] = novoVetor;
        }
        this.matriz = novaMatriz;
    }

    //Setters and Getters

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Matriz matriz1 = (Matriz) o;
        return Arrays.equals(matriz, matriz1.matriz);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(matriz);
    }

    /**
     * c) Método que permite adicionar um elemento no fim de uma determinada linha
     * Se a linha não pertence ao array de Vetores é lançada um excepção
     *
     * @param elemento
     * @param linha
     */
    public void addElementToLine(int elemento, int linha) {
        checkIfLineExists(linha);
        Vetor[] novaMatriz = new Vetor[this.matriz.length];
        for (int i = 0; i < this.matriz.length; i++) {
            Vetor array = this.matriz[i].criarCopia2();
            if (i == linha) //chegando à linha que pretendo adiciono o valor
            {
                array.append(elemento);
            }
            novaMatriz[i] = array;
        }
        this.matriz = novaMatriz;
    }


    private void checkIfLineExists(int linha) {
        Vetor[] novaMatriz = new Vetor[this.matriz.length];
        if (linha < 0 || linha >= novaMatriz.length) {
            throw new IllegalArgumentException("A linha não existe na matriz");
        }
    }

    /**
     * d) Método que retira a primeira ocorrência dado elemento do array de Vetores
     *
     * @param elemento
     */
    public void removeFirstElement(int elemento) {
        int linha = this.numeroDaLinhaDoElemento(elemento);
        if (linha >= 0) {
            Vetor[] novaMatriz = new Vetor[this.matriz.length];
            for (int i = 0; i < this.matriz.length; i++) {
                Vetor array = this.matriz[i].criarCopia2(); // deveria ter .toVetor em vez de criarCopia2 de forma a ser mais percetivel o que o metodo faz
                if (i == linha) {
                    array.removeFirstElement(elemento);
                }
                novaMatriz[i] = array;
            }
            this.matriz = novaMatriz;
        }
    }

    private int numeroDaLinhaDoElemento(int elemento) {
        int linha = -1;
        for (int i = 0; i < this.matriz.length && linha < 0; i++) {
            if (matriz[i].presenteNoVetor(elemento)) {
                linha = i;
            }
        }
        return linha;
    }

    /**
     * f) Método que retorna o maior elemento do array de Vetores
     *
     * @return
     */
    public int highest() {
        int highest = 0;
        Vetor highests = new Vetor(); // os maiores de cada linha
        for (Vetor vetor : this.matriz) {
            highests.append(vetor.highest());
        }
        highest = highests.highest(); // o maior dos maiores (maior do array criado com os maiores)
        return highest;
    }

    /**
     * g) Método que retorna o maior elemento do array de Vetores
     *
     * @return
     */
    public int smalest() {
        int smalest = 0;
        Vetor smalests = new Vetor(); // os menores de cada linha
        for (Vetor vetor : this.matriz) {
            smalests.append(vetor.smalest());
        }
        smalest = smalests.smalest(); // o menor dos menores
        return smalest;
    }

    /**
     * e) Método que torna True se a matriz estiver vazia ou falso em caso contrário
     *
     * @return
     */
    public boolean isMatrixEmpty() {
        if (this.matriz.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * h) Retorne a média dos elementos da matriz
     *
     * @return
     */
    public double matrixElementsAverage() {
        double average = getSumOfValuesInAllLines() / getNumberOfMatrixElementsInAllLines();

        return average;
    }

    private int getSumOfValuesInAllLines() {
        int sum = 0;

        for (int i = 0; i < this.matriz.length; i++) {
            int[] array = this.matriz[i].toArray();
            sum += Utilities.sumElementsOfArray(array);
        }
        return sum;
    }

    private int getNumberOfMatrixElementsInAllLines() {
        int count = 0;

        for (int i = 0; i < this.matriz.length; i++) {
            count += this.matriz[i].numElementos();
        }
        return count;
    }

    /**
     * i) Retorna um vetor em que cada posição
     * corresponde à soma dos elementos da linha homóloga do array encapsulado.
     *
     * @return
     */
    public int[] sumOfMatrixLineElements() {
        int[] sumsArray = new int[this.matriz.length];

        for (int i = 0; i < this.matriz.length; i++) {
            sumsArray[i] = Utilities.sumElementsOfArray((this.matriz[i].toArray()));
        }

        return sumsArray;
    }

    /** j) Retorna um vetor em que cada posição corresponde à soma dos elementos da coluna homóloga
     do array encapsulado
     *
     * @return
     */
    public int[] sumOfMatrixColumnsElements() {
        int[] sumsArray = new int[maxLengthOfColumnInMatrix()];
        int[][] matrix = objToArray();

        for (int j = 0; j < matrix.length; j++) { // linha 225 e 226 representa for( int [] line : matrix)
            int[] line = matrix[j];
            for (int i = 0; i < line.length; i++) {
                sumsArray[i] += line[i];
            }
        }

        return sumsArray;
    }

    /** k) Retorna o índice da linha do array com maior soma dos respetivos elementos. É usado o
     * método da alínea i). Caso o valor da soma de duas ou mais linhas for igual, retorna o index da primeira
     * ocorrência
     * @return
     */
    public int biggestSumLineIndex(){
        int [] lineSums = sumOfMatrixLineElements();
        int index = 0;
        int maior = lineSums [0];

        for (int i = 0; i < lineSums.length; i++) {
            if(maior < lineSums[i]){
                index = i;
            }
        }
        return index;
    }

    /** l) Retorna True se o array encapsulado corresponde a uma matriz quadrada (nxn)
     *
     * @return
     */
    public boolean isMatrixSquare(){
        boolean isMatrixSquare = true;
        for (int i = 0, k = i + 1; k < this.matriz.length && isMatrixSquare; i++, k++) {
            if(this.matriz[i].numElementos() != this.matriz[k].numElementos())
                isMatrixSquare = false;
        }
        return isMatrixSquare;
    }


    private int maxLengthOfColumnInMatrix() {
        int length = this.matriz[0].numElementos();
        for (Vetor row : this.matriz) {
            if (length < row.numElementos()) {
                length = row.numElementos();
            }
        }
        return length;
    }

    private int[][] objToArray() { // método que transforma um objeto Matriz em array de arrays
        int[][] copiaMatriz = new int[this.matriz.length][];
        for (int i = 0; i < this.matriz.length; i++) {
            Vetor arr = new Vetor(this.matriz[i].toArray());
            copiaMatriz[i] = arr.toArray(); //.toArray » método da Class Vetor
        }
        return copiaMatriz;
    }
}