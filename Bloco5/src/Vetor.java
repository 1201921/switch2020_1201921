import java.util.Arrays;

public class Vetor { //Bloco5Ex1

    // atributos

    private int[] vetor;

    // contrutores

    /**
     * a) Método contrutor que inicializa um vetor vazio
     */
    public Vetor() {
        this.vetor = new int[0];    //Definir vetor com 0 elementos
    }

    /**
     * b) Método construtor que inicializa um vetor com elementos
     */
    public Vetor(int[] vetor) {
        validaVetor(vetor);
        this.vetor = criarCopia(vetor);
    }

    //===================================================================================

    /**
     * Adicionado à posteriori com ajuda de um colega
     *
     * @return
     */
    public int[] toArray() {
        int[] copia = new int[this.vetor.length];
        for (int i = 0; i < this.vetor.length; i++) {
            copia[i] = this.vetor[i];
        }
        return copia;
    }

    //===================================================================================

    // setters and getters

    /**
     * c) Testado - Adicionar novo elemento ao vetor encapsulado
     *
     * @param elemento
     * @return array com novo elemento na ultima posição
     */
    public int[] adicionaUmElemento(int elemento) {
        int[] novoVetor = new int[this.vetor.length + 1];
        for (int i = 0; i < this.vetor.length; i++) {
            novoVetor[i] = this.vetor[i];
        }
        novoVetor[this.vetor.length] = elemento;

        return novoVetor;
    }

    /**
     * Adicionado à posteriori com ajuda de um colega
     *
     * @return
     */
    public void append(int valor) {
        int[] copiaMaior = new int[this.vetor.length + 1];
        for (int i = 0; i < this.vetor.length; i++) {
            copiaMaior[i] = this.vetor[i];
        }
        copiaMaior[this.vetor.length] = valor;
        this.vetor = copiaMaior;
    }

    /**
     * d) Testado - Remove o 1º elemento com um determinado valor do array encapsulado
     *
     * @param elemento
     */
    public void removerPrimeiroElemento(int elemento) {

        if (!presenteNoVetor(elemento)) //se nao estiver presente no vetor
            throw new IllegalArgumentException("Elemento não está presente no vetor");

        int[] novoVetor = new int[this.vetor.length - 1];
        int k = 0;

        for (int i = 0; i < this.vetor.length; i++) {
            if (this.vetor[i] == elemento && k == i)
                continue;
            novoVetor[k] = this.vetor[i];
            k++;
        }
        this.vetor=novoVetor;
    }

    public void removeFirstElement(int elemento) {

        if (!presenteNoVetor(elemento)) //se nao estiver presente no vetor
            throw new IllegalArgumentException("Elemento não está presente no vetor");

        int[] novoVetor = new int[this.vetor.length - 1];
        int k = 0;

        for (int i = 0; i < this.vetor.length; i++) {
            if (this.vetor[i] == elemento && k == i)
                continue;
            novoVetor[k] = this.vetor[i];
            k++;
        }
        this.vetor = novoVetor;
    }

    /**
     * e) Testado - Retorna o valor numa determinada posição do vetor
     *
     * @param posicao onde se vai extrair o valor
     * @return
     */
    public int valorNumaPosicao(int posicao) {

        if (posicao > this.vetor.length || posicao < 0)
            throw new IllegalArgumentException("Posição inválida.");

        return this.vetor[posicao];

    }

    /**
     * f) Testado - Retorna o nº de elementos do vetor
     *
     * @return
     */
    public int numElementos() {
        int count = this.vetor.length;

        return count;
    }

    /**
     * g) Testado - Retorna o elemento de maior valor
     *
     * @return
     */
    public int maiorElemento() {
        int maior = 0;

        for (int i = 0; i < this.vetor.length; i++) {
            if (maior < this.vetor[i])
                maior = this.vetor[i];
        }

        return maior;
    }

    /**
     * Adicionado à posteriori com ajuda de um colega
     *
     * @return
     */
    public int highest() {
        int highest = this.vetor[0];
        for (int valor : this.vetor) {
            if (valor > highest)
                highest = valor;
        }
        return highest;
    }

    /**
     * h) Testado - Retorna o elemento de menor valor
     *
     * @return
     */
    public int menorElemento() {
        int menor = this.vetor[0];

        for (int i = 0; i < this.vetor.length; i++) {
            if (menor > this.vetor[i])
                menor = this.vetor[i];
        }
        return menor;
    }

    /**
     * Adicionado à posteriori com ajuda de um colega
     *
     * @return
     */
    public int smalest() {
        int smalest = this.vetor[0];
        for (int valor : this.vetor) {
            if (valor < smalest)
                smalest = valor;
        }
        return smalest;
    }

    /**
     * i) Testado - Retorna a média dos elementos do vetor
     *
     * @param vetor
     * @return
     */
    public double mediaElementosVetor(int[] vetor) {
        double media = somaElementosVetor(vetor) / numElementos();
        return media;
    }

    /**
     * j) Testado - Retorna a média dos elementos pares do vetor original
     *
     * @return
     */
    public double mediaElementosPares() {
        double mediaPares = 0;
        int numElementos = 0;
        int[] vetorCopia = criarCopia(vetor);
        int[] vetorPares = new int[vetorCopia.length];

        for (int i = 0; i < vetorCopia.length; i++) {
            if (vetorCopia[i] % 2 == 0) {
                vetorPares[numElementos] = vetorCopia[i];
                numElementos++;
            }
        }
        int[] novoVetorPares = removerValoresVaziosVetor(vetorPares, numElementos);

        mediaPares = somaElementosVetor(novoVetorPares) / numElementos;

        return mediaPares;
    }

    /**
     * k) Testado - Retorna a média dos elementos impares do vetor original
     *
     * @return
     */
    public double mediaElementosImpares() {
        double mediaImpares;
        int numElementos = 0;
        int[] vetorCopia = criarCopia(vetor);
        int[] vetorImpares = new int[vetorCopia.length];

        for (int i = 0; i < vetorCopia.length; i++) {
            if (vetorCopia[i] % 2 != 0) {
                vetorImpares[numElementos] = vetorCopia[i];
                numElementos++;
            }
        }
        int[] novoVetorPares = removerValoresVaziosVetor(vetorImpares, numElementos);

        mediaImpares = somaElementosVetor(novoVetorPares) / numElementos;

        return mediaImpares;
    }

    /**
     * l) TESTADO - Retorna a média dos elementos de um vetor múltiplos de um numero
     *
     * @return
     */
    public double mediaElementosMultiplosDeUmNumero(int numero) {
        int[] vetorCopia = criarCopia(this.vetor);
        int[] vetorMultiplosDeUmNumero = new int[vetorCopia.length];
        int numElementos = 0;

        for (int i = 0; i < vetorCopia.length; i++) {
            if (vetorCopia[i] % numero == 0) {
                vetorMultiplosDeUmNumero[numElementos] = vetorCopia[i];
                numElementos++;
            }
        }

        int[] novoVetorMultiplos = removerValoresVaziosVetor(vetorMultiplosDeUmNumero, numElementos);

        double media = somaElementosVetor(novoVetorMultiplos) / numElementos;
        return media;
    }

    /**
     * m) Testado - Retorna uma cópia ordenada de forma crescente do vetor
     *
     * @return
     */
    public int[] ordenaCrescente() {
        int[] vetorCopia = criarCopia(this.vetor);
        int temp = 0;

        for (int i = 0; i < vetorCopia.length; i++) {
            for (int j = i + 1; j < vetorCopia.length; j++) {
                if (vetorCopia[i] > vetorCopia[j]) {
                    temp = vetorCopia[i];
                    vetorCopia[i] = vetorCopia[j];
                    vetorCopia[j] = temp;
                }
            }
        }
        return vetorCopia;
    }

    /**
     * m) Testado - Retorna uma cópia ordenada de forma decrescente do vetor
     *
     * @return
     */
    public int[] ordenaDecrescente() {
        int[] vetorCopia = criarCopia(this.vetor);
        int temp = 0;

        for (int i = 0; i < vetorCopia.length; i++) {
            for (int j = i + 1; j < vetorCopia.length; j++) {
                if (vetorCopia[i] < vetorCopia[j]) {
                    temp = vetorCopia[i];
                    vetorCopia[i] = vetorCopia[j];
                    vetorCopia[j] = temp;
                }
            }
        }
        return vetorCopia;
    }

    /**
     * n) Testado - Retorna true se vetor vazio / false em caso contrário
     *
     * @return
     */
    public boolean verificaVetorVazio() {
        if (this.vetor.length == 0)
            return true;
        return false;
    }

    /**
     * o) Testado - Retorna true se vetor contiver apenas um elemeto / false em caso contrário
     *
     * @return
     */
    public boolean verificaVetorComUmElemento() {
        if (this.vetor.length == 1)
            return true;
        return false;
    }

    /**
     * p) Testado - Retorna true se vetor contém apenas um elemento par / false em caso contrário
     *
     * @return
     */
    public boolean verificaVetorComUmElementoPar() {
        if (this.vetor.length == 1 && this.vetor[0] % 2 == 0)
            return true;
        return false;
    }

    /**
     * q) Testado - Retorna true se vetor contém apenas um elemento impar / false em caso contrário
     *
     * @return
     */
    public boolean verificaVetorComUmElementoImpar() {
        if (this.vetor.length == 1 && this.vetor[0] % 2 != 0)
            return true;
        return false;
    }

    /**
     * r) Testado - Retorna true se o vetor contém nºs duplicados / false em caso contrário
     *
     * @return
     */
    public boolean verificaElementosDuplicadosNoVetor() {
        for (int i = 0; i < this.vetor.length; i++) {
            for (int j = i + 1; j < this.vetor.length; j++) {
                if (this.vetor[i] == this.vetor[j])
                    return true;
            }
        }
        return false;
    }

    /**
     * s) Testado - Retorna os elementos do vetor original cujo nº de algarismos é superior ao nº
     * médio de algarismos de todos os elementos do vetor
     *
     * @return
     */
    public int[] elementosComMaisAlgarismosQueAMediaDeAlgarismos() {
        int numAlgarismos = 0;
        int[] copiaVetor = criarCopia(this.vetor);
        int algarismos[] = new int[this.vetor.length];

        //contar algarismos dos elementos do vetor
        for (int i = 0; i < this.vetor.length; i++) {
            numAlgarismos = 0;
            for (int j = 0; copiaVetor[i] > 0; j++) {
                copiaVetor[i] = copiaVetor[i] / 10;
                numAlgarismos++;
            }
            algarismos[i] = numAlgarismos;
        }
        double mediaAlgarismos = mediaElementosVetor(algarismos);

        int[] elementosRetornar = new int[copiaVetor.length];

        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (algarismos[i] > mediaAlgarismos) {
                elementosRetornar[k] = this.vetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(elementosRetornar, k);
    }

    /**
     * t) Testado - Retorna os elementos do vetor cuja percentagem de algarismos pares é superior à
     * média da percentagem de algarismos pares de todos os elementos do vetor
     *
     * @return
     */
    public int[] elementosParesComMaisAlgarismosQueAMediaDeAlgarismos() {
        int numAlgarismos = 0, numAlgarismosPar = 0;
        int[] copiaVetor = criarCopia(this.vetor);
        int[] percentagemAlgarismosPares = new int[this.vetor.length];

        //contar algarismos pares dos elementos do vetor
        for (int i = 0; i < this.vetor.length; i++) {
            numAlgarismos = 0;
            numAlgarismosPar = 0;
            for (int j = 0; copiaVetor[i] > 0; j++) {
                numAlgarismos++;
                if (copiaVetor[i] % 2 == 0) {
                    numAlgarismosPar++;
                }
                copiaVetor[i] = copiaVetor[i] / 10;
            }
            percentagemAlgarismosPares[i] = numAlgarismosPar * 100 / numAlgarismos;
        }

        double mediaAlgarismosPares = mediaElementosVetor(percentagemAlgarismosPares);

        int[] elementosRetornar = new int[copiaVetor.length];

        int k = 0;
        for (int i = 0; i < this.vetor.length; i++) {
            if (percentagemAlgarismosPares[i] > mediaAlgarismosPares) {
                elementosRetornar[k] = this.vetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(elementosRetornar, k);
    }

    /**
     * u) Testado - Retorna os elementos do vetor compostos exclusivamente por algarismos pares
     *
     * @return
     */
    public int[] elementosApenasComAlgarismosPares() {
        int numAlgarismos = 0, numAlgarismosPar = 0;
        int[] copiaVetor = criarCopia(this.vetor);
        int[] percentagemAlgarismosPares = new int[this.vetor.length];

        //contar algarismos pares dos elementos do vetor
        for (int i = 0; i < this.vetor.length; i++) {
            numAlgarismos = 0;
            numAlgarismosPar = 0;
            for (int j = 0; copiaVetor[i] > 0; j++) {
                numAlgarismos++;
                if (copiaVetor[i] % 2 == 0) {
                    numAlgarismosPar++;
                }
                copiaVetor[i] = copiaVetor[i] / 10;
            }
            percentagemAlgarismosPares[i] = numAlgarismosPar * 100 / numAlgarismos;
        }

        int[] elementosRetornar = new int[copiaVetor.length];

        int k = 0;
        for (int i = 0; i < this.vetor.length; i++) {
            if (percentagemAlgarismosPares[i] == 100) {
                elementosRetornar[k] = this.vetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(elementosRetornar, k);
    }

    /**
     * v) Testado - Retorna os elementos que são sequências crescentes (e.g. 347) do vetor.
     *
     * @return
     */
    public int[] elementosSequenciaCrescente() {
        int[] copiaVetor = criarCopia(this.vetor);
        int[] sequenciasCrescentes = new int[copiaVetor.length];
        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (verificaOrdemCrescente(copiaVetor[i])) {
                sequenciasCrescentes[k] = copiaVetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(sequenciasCrescentes, k);
    }

    /**
     * w) Testado - Retorna as capicuas existentes no vetor
     *
     * @return
     */
    public int[] elementosCapicua() {
        int[] copiaVetor = criarCopia(this.vetor);
        int[] capicuas = new int[copiaVetor.length];
        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (verificaIgualdadeVetorInverso(copiaVetor[i])) {
                capicuas[k] = copiaVetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(capicuas, k);
    }

    /**
     * Testado - x) Retorne os números existentes no vetor compostos exclusivamente por um mesmo algarismo (e.g. 222)
     *
     * @return
     */
    public int[] elementosCompostosPorUmAlgarismo() {
        int[] copiaVetor = criarCopia(this.vetor);
        int[] algarismosIguais = new int[copiaVetor.length];
        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (verificaIgualdadeDosAlgarismos(copiaVetor[i])) {
                algarismosIguais[k] = copiaVetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(algarismosIguais, k);
    }

    /**
     * Testado - y) Retorne os números existentes no vetor que não são de Amstrong.
     *
     * @return
     */
    public int[] elementosNumerosNaoAmstrong() {
        int[] copiaVetor = criarCopia(this.vetor);
        int[] elementosNaoAmstrong = new int[copiaVetor.length];
        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (!verificaNumeroAmstrong(copiaVetor[i])) {
                elementosNaoAmstrong[k] = copiaVetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(elementosNaoAmstrong, k);
    }

    /**
     * Testado - z) Retorna os elementos que contêm uma sequência crescente de pelo menos n algarismos (e.g. n=3, 347) do vetor
     *
     * @param n - nº de algarismos minimo que a sequencia crescente deve conter
     * @return
     */
    public int[] elementosComSequenciaCrescenteDeNAlgarismos(int n) {
        int[] copiaVetor = criarCopia(this.vetor);
        int[] elementosRetornar = new int[copiaVetor.length];
        int k = 0;
        for (int i = 0; i < copiaVetor.length; i++) {
            if (verificaSequenciaCrescenteDeNAlgarismos(copiaVetor[i], n)) {
                elementosRetornar[k] = copiaVetor[i];
                k++;
            }
        }
        return removerValoresVaziosVetor(elementosRetornar, k);
    }

    /**
     * Testado - aa) Retorna True ou False, consoante o vetor original é igual a um vetor passado por parâmetro
     *
     * @param vetorParametro
     * @return
     */
    public boolean igualdadeVetorOriginalVetorParametro(int[] vetorParametro) {
        if (Arrays.equals(this.vetor, vetorParametro))
            return true;
        return false;

    }

    // outros métodos

    private void validaVetor(int[] vetor) {
        if (vetor == null) //|| vetor.length == 0
            throw new IllegalArgumentException("O vetor não é válido.");
    }

    public boolean presenteNoVetor(int elemento) {
        boolean presente = false;
        for (int i = 0; i < this.vetor.length && !presente; i++) {
            if (this.vetor[i] == elemento)
                presente = true;
        }
        return presente;
    }

    private int[] criarCopia(int[] vetor) {
        int[] copia = new int[vetor.length];
        for (int i = 0; i < vetor.length; i++) {
            copia[i] = vetor[i];
        }
        return copia;
    }

    public Vetor criarCopia2() {
        int[] copia = new int[vetor.length];
        for (int i = 0; i < vetor.length; i++) {
            copia[i] = vetor[i];
        }
        Vetor copia2 = new Vetor(copia);
        return copia2;
    }

    private int[] removerValoresVaziosVetor(int[] vetor, int count) {
        int[] novoVetor = new int[count];

        for (int i = 0; i < count; i++) {
            novoVetor[i] = vetor[i];
        }
        return novoVetor;
    }

    public double somaElementosVetor(int[] vetor) {
        int soma = 0;
        for (int i = 0; i < vetor.length; i++) {
            soma += vetor[i];
        }
        return soma;
    }

    public double somaElementosVetor2() {
        int soma = 0;
        for (int valor : this.vetor) {
            soma += valor;
        }
        return soma;
    }

    private int[] transformarNumeroEmArray(int valor) {
        int array[] = new int[5];
        int count = 0;
        for (int i = 0; valor > 0; i++) {
            array[i] = valor % 10;
            valor = valor / 10;
            count++;
        }
        int arrayCopy[] = new int[count];
        for (int i = 0; i < arrayCopy.length; i++) {
            arrayCopy[i] = array[i];
        }

        int[] arrayCopy2 = new int[arrayCopy.length];
        for (int i = 0, k = arrayCopy.length - 1; i < arrayCopy.length; i++, k--) {
            arrayCopy2[k] = arrayCopy[i];
        }

        return arrayCopy2;
    }

    private boolean verificaOrdemCrescente(int valor) {
        int[] array = transformarNumeroEmArray(valor);
        boolean a = false;
        for (int i = array.length - 1, k = array.length - 2; k >= 0 && array[i] > array[k]; i--, k--) {
            if (array[i] > array[k]) {
                a = true;
            }
        }
        return a;
    }

    private boolean verificaIgualdadeVetorInverso(int valor) {
        int[] array = transformarNumeroEmArray(valor);
        int[] arrayInverso = new int[array.length];
        boolean a = false;
        for (int i = 0, k = array.length - 1; i < array.length; i++, k--) {
            arrayInverso[i] = array[k];
        }
        if (Arrays.equals(array, arrayInverso))
            a = true;
        return a;
    }

    private boolean verificaIgualdadeDosAlgarismos(int valor) {
        int[] array = transformarNumeroEmArray(valor);
        boolean a = true;

        int i = 1, k = i - 1;
        do {
            if (array[i] != array[k]) {
                a = false;
            }
            i++;
            k++;
        } while (i < array.length && a == true);

        return a;
    }

    private boolean verificaNumeroAmstrong(int valor) {
        //amstrong = soma dos cubos dos seus algarismos
        int[] array = transformarNumeroEmArray(valor);
        int somaCubos = 0;

        for (int i = 0; i < array.length; i++) {
            somaCubos += Math.pow(array[i], 3);
        }
        if (valor != somaCubos) return false;
        return true;
    }

    private boolean verificaSequenciaCrescenteDeNAlgarismos(int valor, int n) {
        int[] array = transformarNumeroEmArray(valor);
        int a = 0; //nº vezes em que o valor seguinte é maior que o anterior
        for (int i = 1, k = 0; i < array.length && a != n - 1; i++, k++) {
            if (array[i - 1] < array[i])
                a++;
            else a = 0;
        }
        if (a >= n - 1)
            return true;
        else return false;
    }

    /*//======================== Comparar 2 objetos
    // (Generate>equals)
    public boolean equals (Object obj){ //"obj" é o nome que dei ao objeto
        if(obj==this) //se pertence à mesma zona de memória
            return true;
        if(!obj instanceof VetorBiDimensional(ou this) == false) // se o objeto a chegar pelo paramentro é igual à instancia, logo comparavel/compativel
            return false;
        VetorBiDimensional outroObj = (VetorBiDimensional)obj; //1ªs 4 linhas são genéricas e são sempre usadas (copiar-colar)
        this.vectorBi == outro.vectorBi; //vectorBi é o atributo; esta linha
    }*/

    @Override
    public boolean equals(Object other) {
        if (this.vetor == other) return true;
        if (other == null || this.getClass() != other.getClass()) return false;

        Vetor otherObj = (Vetor) other;
        if (this.vetor.length != otherObj.vetor.length)
            return false;
        boolean isEquals = true;
        for (int i = 0; i < this.vetor.length && isEquals; i++) {
            //isEquals = this.vetor[i] == otherObj.vetor[i]; // » esta linha representa as próximas 4
            // isEquals é igual à comparação entre this.vetor e otherObj
            if (this.vetor[i] == otherObj.vetor[i])
                isEquals = true;
            else isEquals = false;
        }
        return isEquals;
    }

}