public class Utilities {

    public static double averageElementsOfArray(int[] arrayNumbers) {
        double average;

        average = roundNumber(sumElementsOfArray(arrayNumbers) / arrayNumbers.length, 2);

        return average;
    }

    public static int sumElementsOfArray(int[] numberList) {
        int sum = 0;

        for (int i = 0; i < numberList.length; i++) {
            sum += numberList[i];
        }

        return sum;
    }

    public static double roundNumber(double number, int decimalDigits) {
        double result;

        result = Math.round(number * Math.pow(10, decimalDigits)) / (Math.pow(10, decimalDigits));

        return result;
    }

    /*public static int intSumElementsOfArray(int[] numberList) {
        int sum = 0;

        for (int i = 0; i < numberList.length; i++) {
            sum += numberList[i];
        }

        return sum;
    }*/

    public static boolean evenNumber(int number) {
        boolean even = false;
        if (number < 0) {
            number = -number;
        }

        if (number % 2 == 0) {
            return true;
        }
        return even;
    }

    public static boolean oddNumber(int number) {
        boolean odd = false;

        if (number < 0) {
            number = -number;
        }

        if (number % 2 == 1) {
            return true;
        }
        return odd;
    }

    public static Double getMeanOfMatrix(int[][] matrix) {
        double sum = 0;
        double totalElements = 0;
        double mean;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sum += matrix[i][j];
            }
            totalElements += matrix[i].length;
        }

        mean = roundNumber(sum / totalElements, 2);

        return mean;
    }


}
