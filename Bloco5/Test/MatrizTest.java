import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

class MatrizTest {

    @Test
    void checkMatrizNula() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matriz myMatriz = new Matriz((Vetor[]) null);
        });
    }


    @Test
    void checkIfLineExists() {
        //Arrange
        int[][] vetor = {{1, 2, 3}, {2, 34, 4}, {2, 4, 5}};
        Matriz result = new Matriz(vetor);
        int linha = 6;
        int elemento = 0;

        //Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
        {
            result.addElementToLine(elemento, linha);
        });

    }

    @Test
    void addElementToLine() {
        //Arrange
        int[][] vetor = {{1, 2, 3}, {2, 34, 4}, {2, 4, 5}};
        Matriz result = new Matriz(vetor);
        int linha = 1;
        int elemento = 4;
        int[][] vetorExpected = {{1, 2, 3}, {2, 34, 4, 4}, {2, 4, 5}};
        Matriz expected = new Matriz(vetorExpected);

        //Act
        result.addElementToLine(elemento, linha);

        //Assert
        Assert.assertEquals(expected, result);
    }

    @Test
    void removeFirstElement() {
        //Arrange
        int[][] vetor = {{1, 2, 3}, {2, 34, 4}, {2, 4, 5}};
        Matriz result = new Matriz(vetor);
        int elemento = 4;
        int[][] vetorExpected = {{1, 2, 3}, {2, 34}, {2, 4, 5}};
        Matriz expected = new Matriz(vetorExpected);

        //Act
        result.removeFirstElement(elemento);

        //Assert
        Assert.assertEquals(expected, result);
    }

    @Test
    void highest() {
        //Arrange
        int[][] matriz = {
                {1, 2, 3, 4, 5},
                {8, 3, 1, 22, 2},
                {5, 7, 6, 7}};
        Matriz matrix = new Matriz(matriz);
        int expected = 22;
        //Act
        int result = matrix.highest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void smalest() {
        //Arrange
        int[][] matriz = {
                {1, 2, 3, 4, 5},
                {8, 3, 2, 22, 2},
                {5, 7, 6, 7}};
        Matriz matrix = new Matriz(matriz);
        int expected = 1;
        //Act
        int result = matrix.smalest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixEmptyTrue() {
        //Arrange
        int[][] emptyMatrix = {};
        Matriz matrix = new Matriz(emptyMatrix);
        boolean expected = true;
        //Act
        boolean result = matrix.isMatrixEmpty();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixEmptyFalse() {
        //Arrange
        int[][] emptyMatrix = {{1, 2, 3}, {2, 3, 1}};
        Matriz matrix = new Matriz(emptyMatrix);
        boolean expected = false;
        //Act
        boolean result = matrix.isMatrixEmpty();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void matrixElementsAverage() {
        //arrange
        int[][] matrix = {{1, 2, 3}, {2, 3, 1}};
        Matriz matrix2 = new Matriz(matrix);
        double expected = 2.0;
        //act
        double result = matrix2.matrixElementsAverage();
        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void sumOfLineElementsOfTheArray() {
        //arrange
        int[][] matrix = {{1, 2, 3}, {2, 3, 1}};
        Matriz matrix2 = new Matriz(matrix);
        int[] expected = {6, 6};
        //act
        int[] result = matrix2.sumOfMatrixLineElements();
        //assert
        assertArrayEquals(expected, result);
    }

    @Test
    void sumOfMatrixColumnsElements() {
        //arrange
        int[][] matrix = {{3, 4}, {4, 2, 5}};
        Matriz objMatrix = new Matriz(matrix);
        int[] expected = {7, 6, 5};
        //act
        int[] result = objMatrix.sumOfMatrixColumnsElements();
        //assert
        assertArrayEquals(expected, result);
    }

    @Test
    void biggestSumLineIndex() {
        //arrange
        int[][] matrix = {{3, 4}, {4, 8, 5}, {10, 20, 1}};
        Matriz objMatrix = new Matriz(matrix);
        int expected = 2;
        //act
        int result = objMatrix.biggestSumLineIndex();
        //assert
        assertEquals(expected, result);
    }


    @Test
    void isMatrixSquareTrue() {
        //arrange
        int[][] matrix = {{3, 4, 3}, {4, 8, 5}, {10, 20, 1}};
        Matriz objMatrix = new Matriz(matrix);
        boolean expected = true;
        //act
        boolean result = objMatrix.isMatrixSquare();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareFalse() {
        //arrange
        int[][] matrix = {{4, 1, 8}, {4, 8, 5}, {10, 20}};
        Matriz objMatrix = new Matriz(matrix);
        boolean expected = false;
        //act
        boolean result = objMatrix.isMatrixSquare();
        //assert
        assertEquals(expected, result);
    }
}