import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class VetorTest { //Arrange//Act//Assert

    @Test
    void testarContrutorNulo () {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Vetor myVetor6 = new Vetor(null);});
    }

    @Test //Tinha este método como void e nao sei testar
    void removerPrimeiroElementoComDoisElementosIguais() {
        //Arrange
        int [] vetor = {1,2,3,4,3,6};
        Vetor result = new Vetor (vetor);
        int elemento = 3;
        int [] vetorExpected = {1,2,4,3,6};
        Vetor expected = new Vetor (vetorExpected);
        //Act
        result.removerPrimeiroElemento(elemento);
        //Assert
        assertEquals(expected,result);
    }

    @Test
    void removerElementoNaoPresente() {
        int [] vetor = {1,2,3,4,3,6};
        int elemento = 7;
        Vetor myVetor2 = new Vetor (vetor);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            myVetor2.removerPrimeiroElemento(elemento);});
    }

    @Test
    void adicionaUmElemento() {
        int [] vetor = {1,2,3,4,3,6};
        int elemento = 7;
        Vetor myVetor2 = new Vetor (vetor);

        int [] expected = {1,2,3,4,3,6,7};
        int [] result = myVetor2.adicionaUmElemento(elemento);
        assertArrayEquals(expected, result);
    }

    @Test
    void valorNumaPosicaoValida () {
        int[] vetor = {1, 2, 3, 4, 3, 6};
        int posicao = 0;
        Vetor myVetor2 = new Vetor(vetor);

        int expected = 1;
        int result = myVetor2.valorNumaPosicao(posicao);
        assertEquals(expected, result);
    }

    @Test
    void valorNumaPosicaoInvalidaInferior0 (){
        int[] vetor = {1, 2, 3, 4, 3, 6};
        int posicao = -1;
        Vetor myVetor2 = new Vetor(vetor);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            myVetor2.valorNumaPosicao(posicao);});
    }

    @Test
    void valorNumaPosicaoInvalidaSuperiorAoLength (){
        int[] vetor = {1, 2, 3, 4, 3, 6};
        int posicao = 10;
        Vetor myVetor2 = new Vetor(vetor);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            myVetor2.valorNumaPosicao(posicao);});
    }

    @Test
    void numElementoDoVetor(){
        int[] vetor = {1, 2, 3, 4, 3, 6};
        Vetor myVetor2 = new Vetor (vetor);

        int expected = 6;
        int result = myVetor2.numElementos();
        assertEquals(expected,result);
    }

    @Test
    void numElementoDoVetorVazio(){
        int[] vetor = {};
        Vetor myVetor2 = new Vetor (vetor);

        int expected = 0;
        int result = myVetor2.numElementos();
        assertEquals(expected,result);

        //Assertions.assertThrows(IllegalArgumentException.class, () -> {myVetor2.numElementos ());});

    }

    @Test
    void maiorElemento (){
        int[] vetor = {1, 2, 3, 4, 3, 6};
        Vetor myVetor2 = new Vetor (vetor);

        int expected = 6;
        int result = myVetor2.maiorElemento();
        assertEquals(expected,result);

    }

    @Test
    void menorElemento (){
        int[] vetor = {1, 2, 3, 4, 3, 6};
        Vetor myVetor2 = new Vetor (vetor);

        int expected = 1;
        int result = myVetor2.menorElemento();
        assertEquals(expected,result);
    }

    @Test
    void mediaElementosMultiplosDeUmNumero() {

        int [] vetor = {1,2,3,4,5,6};
        int numero = 3;
        Vetor myVetor = new Vetor(vetor); //crio um novo vetor que vai ser testado

        double expected = 4.5;
        double result = myVetor.mediaElementosMultiplosDeUmNumero(numero); //método em que passo o argumento
        assertEquals(expected, result);
    }

    @Test
    void mediaElementosPares(){
        int [] vetor = {1,2,3,4,5,6};
        Vetor myVetor = new Vetor (vetor);

        double expected = 4.0;
        double result = myVetor.mediaElementosPares();
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaElementosImpares(){
        int [] vetor = {1,2,3,4,5,6};
        Vetor myVetor = new Vetor (vetor);

        double expected = 3.0;
        double result = myVetor.mediaElementosImpares();
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ordenaCrescente(){
        int [] vetor = {4,2,5,1,3};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {1,2,3,4,5};
        int [] result = myVetor.ordenaCrescente();
        assertArrayEquals(expected, result);
    }

    @Test
    void ordenaDecrescente(){
        int [] vetor = {4,2,5,1,3};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {5,4,3,2,1};
        int [] result = myVetor.ordenaDecrescente();
        assertArrayEquals(expected, result);
    }

    @Test
    void verificaVetorVazioTrue(){
        int [] vetor = {};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = true;
        boolean result = myVetor.verificaVetorVazio();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorVazioFalse(){
        int [] vetor = {1};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaVetorVazio();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElemento(){
        int [] vetor = {1};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = true;
        boolean result = myVetor.verificaVetorComUmElemento();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElemento_vetorVazio(){
        int [] vetor = {};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaVetorComUmElemento();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElemento_vetorComMaisDoQueUmElemento(){
        int [] vetor = {1,2,3};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaVetorComUmElemento();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElementoParTrue(){
        int [] vetor = {2};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = true;
        boolean result = myVetor.verificaVetorComUmElementoPar();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElementoParFalse(){
        int [] vetor = {3};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaVetorComUmElementoPar();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElementoImparFalse(){
        int [] vetor = {2};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaVetorComUmElementoImpar();
        assertEquals(expected, result);
    }

    @Test
    void verificaVetorComUmElementoImparTrue(){
        int [] vetor = {1};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = true;
        boolean result = myVetor.verificaVetorComUmElementoImpar();
        assertEquals(expected, result);
    }

    @Test
    void verificaElementosDuplicadosNoVetorTrue(){
        int [] vetor = {1,2,3,4,3,5};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = true;
        boolean result = myVetor.verificaElementosDuplicadosNoVetor();
        assertEquals(expected, result);
    }

    @Test
    void verificaElementosDuplicadosNoVetorFalse(){
        int [] vetor = {1,2,3,4,5};
        Vetor myVetor = new Vetor (vetor);

        boolean expected = false;
        boolean result = myVetor.verificaElementosDuplicadosNoVetor();
        assertEquals(expected, result);
    }

    @Test
    void elementosComMaisAlgarismosQueAMediaDeAlgarismosTest1(){
        int [] vetor = {138, 26, 24, 46};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {138};
        int [] result = myVetor.elementosComMaisAlgarismosQueAMediaDeAlgarismos();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosComMaisAlgarismosQueAMediaDeAlgarismosTest2(){
        int [] vetor = {38, 26, 24, 46};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {};
        int [] result = myVetor.elementosComMaisAlgarismosQueAMediaDeAlgarismos();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosParesComMaisAlgarismosQueAMediaDeAlgarismosTest1(){
        int [] vetor = {138, 26, 24, 46};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {26,24,46};
        int [] result = myVetor.elementosParesComMaisAlgarismosQueAMediaDeAlgarismos();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosParesComMaisAlgarismosQueAMediaDeAlgarismosTest2(){
        int [] vetor = {138, 25, 34, 46};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {46};
        int [] result = myVetor.elementosParesComMaisAlgarismosQueAMediaDeAlgarismos();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosApenasComAlgarismosPares() {
        int [] vetor = {138, 25, 44, 66};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {44, 66};
        int [] result = myVetor.elementosApenasComAlgarismosPares();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosApenasComAlgarismosParesTest2() {
        int [] vetor = {138, 25, 43, 65};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {};
        int [] result = myVetor.elementosApenasComAlgarismosPares();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosSequenciaCrescente(){
        int [] vetor = {211, 347, 456};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {347, 456};
        int [] result = myVetor.elementosSequenciaCrescente();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosCapicua(){
        int [] vetor = {211, 121, 332, 1221};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {121, 1221};
        int [] result = myVetor.elementosCapicua();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosCompostosPorUmAlgarismo(){
        int [] vetor = {122, 111, 22, 345};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {111, 22};
        int [] result = myVetor.elementosCompostosPorUmAlgarismo();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosNumerosNaoAmstrong(){
        int [] vetor = {57, 370};
        Vetor myVetor = new Vetor (vetor);

        int [] expected = {57};
        int [] result = myVetor.elementosNumerosNaoAmstrong();
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosComSequenciaCrescenteDeNAlgarismosTest1() {
        int[] vetor = {52571, 3245, 121};
        Vetor myVetor = new Vetor (vetor);
        int n = 2;

        int [] expected = {52571, 3245, 121};
        int [] result = myVetor.elementosComSequenciaCrescenteDeNAlgarismos(n);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosComSequenciaCrescenteDeNAlgarismosTest2() {
        int[] vetor = {52571, 3245, 121};
        Vetor myVetor = new Vetor (vetor);
        int n = 3;

        int [] expected = {52571, 3245};
        int [] result = myVetor.elementosComSequenciaCrescenteDeNAlgarismos(n);
        assertArrayEquals(expected, result);
    }

    @Test
    void igualdadeVetorOriginalVetorParametroFalse() {
        int[] vetor = {56, 123, 22};
        Vetor myVetor = new Vetor (vetor);
        int [] vetorParametro = {44, 112};

        boolean expected = false;
        boolean result = myVetor.igualdadeVetorOriginalVetorParametro(vetorParametro);
        assertEquals(expected, result);
    }

    @Test
    void igualdadeVetorOriginalVetorParametroTrue() {
        int[] vetor = {44, 112};
        Vetor myVetor = new Vetor (vetor);
        int [] vetorParametro = {44, 112};

        boolean expected = true;
        boolean result = myVetor.igualdadeVetorOriginalVetorParametro(vetorParametro);
        assertEquals(expected, result);
    }


}
/*@Test
    void verificaIgualdadeVetor() {
        int valor = 1221;

        boolean expected = true;
        boolean result = Vetor.verificaIgualdadeVetor(valor);
        assertEquals(expected, result);
    }*/