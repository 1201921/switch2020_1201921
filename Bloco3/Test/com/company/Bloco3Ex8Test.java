package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex8Test {

    @Test
    void menorNumDaSomaAcumuladaSupAoNumUtilizadorTest1() {
        double[] numerosPositivos = {4, 2.5, 7.8, 11, 1, 2.6, 6.2};
        double numUtilizador = 28;

        double expected = 1;
        double result = Bloco3Ex8.menorNumDaSomaAcumuladaSupAoNumUtilizador(numUtilizador, numerosPositivos);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void menorNumDaSomaAcumuladaSupAoNumUtilizadorTest2() {
        double[] numerosPositivos = {12, 7.5, 20.2, 14, 8, 5.5, 33};
        double numUtilizador = 67;

        double expected = 5.5;
        double result = Bloco3Ex8.menorNumDaSomaAcumuladaSupAoNumUtilizador(numUtilizador, numerosPositivos);
        assertEquals(expected, result, 0.01);
    }
    @Test
    void menorNumDaSomaAcumuladaSupAoNumUtilizadorTest3() {
        double[] numerosPositivos = {12, 7.5, 20.2, 14, 8, 5.5, 33};
        double numUtilizador = 102;

        double expected = 5.5;
        double result = Bloco3Ex8.menorNumDaSomaAcumuladaSupAoNumUtilizador(numUtilizador, numerosPositivos);
        assertEquals(expected, result, 0.01);
    }
}