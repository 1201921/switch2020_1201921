package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex20Test {

    @Test
    void getTipoNumAbundanteTest1() {
        int num = 20;

        String expected = "20 é um nº Abundante.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }

    @Test
    void getTipoNumAbundanteTest2() {
        int num = 30;

        String expected = "30 é um nº Abundante.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }

    @Test
    void getTipoNumPerfeitoTest1() {
        int num = 6;

        String expected = "6 é um nº Perfeito.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }

    @Test
    void getTipoNumPerfeitoTest2() {
        int num = 28;

        String expected = "28 é um nº Perfeito.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }

    @Test
    void getTipoNumReduzidoTest1() {
        int num = 8;

        String expected = "8 é um nº Reduzido.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }

    @Test
    void getTipoNumReduzidoTest2() {
        int num = 101;

        String expected = "101 é um nº Reduzido.";
        String result = Bloco3Ex20.getTipoNum(num);
        assertEquals(expected, result);
    }
}