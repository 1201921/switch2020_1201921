package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex17Test {

    @Test
    void verificarAdequacaoRacaoTest1() {
        double pesoAnimal = 45;
        int qntRacao = 300;

        String expected = "Quantidade de ração adequada.";
        String result = Bloco3Ex17.verificarAdequacaoRacao(pesoAnimal, qntRacao);
    }

    @Test
    void verificarAdequacaoRacaoTest2() {
        double pesoAnimal = 35;
        int qntRacao = 275;

        String expected = "Quantidade de ração não adequada.";
        String result = Bloco3Ex17.verificarAdequacaoRacao(pesoAnimal, qntRacao);
    }

    @Test
    void verificarAdequacaoRacaoTest3() {
        double pesoAnimal = 55;
        int qntRacao = 500;

        String expected = "Quantidade de ração adequada.";
        String result = Bloco3Ex17.verificarAdequacaoRacao(pesoAnimal, qntRacao);
    }

    @Test
    void verificarAdequacaoRacaoTest4() {
        double pesoAnimal = 14;
        int qntRacao = 200;

        String expected = "Quantidade de ração não adequada.";
        String result = Bloco3Ex17.verificarAdequacaoRacao(pesoAnimal, qntRacao);
    }
}