package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3ex4Test {

    @Test
    void multiplos3Test1() {
        int numeros [] = {2, 4, 6, 9};

        int expected = 2;
        int result = Bloco3ex4.multiplos3(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplos3Test2() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};

        int expected = 7;
        int result = Bloco3ex4.multiplos3(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplosDadoNumTest1() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};
        int dadoNum = 3;

        int expected = 7;
        int result = Bloco3ex4.multiplosDadoNum(numeros, dadoNum);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplosDadoNumTest2() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};
        int dadoNum = 2;

        int expected = 6;
        int result = Bloco3ex4.multiplosDadoNum(numeros, dadoNum);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplos3e5Test1() {
        int numeros [] = {2, 4, 6, 9};

        int expected = 0;
        int result = Bloco3ex4.multiplos3e5(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplos3e5Test2() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};

        int expected = 1;
        int result = Bloco3ex4.multiplos3e5(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplosDoisNumerosTest1() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};
        int dadoNum1=3;
        int dadoNum2=5;

        int expected = 1;
        int result = Bloco3ex4.multiplosDoisNumeros(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplosDoisNumerosTest2() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};
        int dadoNum1=4;
        int dadoNum2=2;

        int expected = 2;
        int result = Bloco3ex4.multiplosDoisNumeros(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaMultiplosTest1() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 22, 33, 48};
        int dadoNum1=4;
        int dadoNum2=2;

        int expected = 52;
        int result = Bloco3ex4.somaMultiplos(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaMultiplosTest2() {
        int numeros [] = {2, 4, 6, 9, -3, 15, 18, 30, 33, -60};
        int dadoNum1=3;
        int dadoNum2=5;

        int expected = -15;
        int result = Bloco3ex4.somaMultiplos(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }
}