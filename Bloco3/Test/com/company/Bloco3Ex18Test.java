package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex18Test {


    @Test
    void validarNumCC_comNumReal() {
        int numCC = 9784068, numVerificacao = 8;

        String expected = "Dados de CC/BI corretos";
        String result = Bloco3Ex18.validarNumCC(numCC, numVerificacao);
        assertEquals(expected, result);
    }

    @Test
    void validarNumCC_comNumImaginado() {
        int numCC = 13258478, numVerificacao = 5;

        String expected = "Dados de CC/BI incorretos";
        String result = Bloco3Ex18.validarNumCC(numCC, numVerificacao);
        assertEquals(expected, result);
    }
}