package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex11Test {

    @Test
    void getNumSomasTest1() {
        int expected = 3; //0+5; 5+0; 1+4; 4+1; 2+3; 3+2
        int result = Bloco3Ex11.getNumSomas(5);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumSomasTest2() {
        int expected = 4; //0+6; 6+0; 1+5; 5+1; 2+4; 4+2; 3+3;
        int result = Bloco3Ex11.getNumSomas(6);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumSomasTest3() {
        int expected = 6;
        int result = Bloco3Ex11.getNumSomas(10);
        assertEquals(expected, result, 0.01);
    }
}