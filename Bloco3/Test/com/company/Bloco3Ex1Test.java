package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex1Test {

    @Test
    void getResultadoNum4() {
        int res=1, num=4;

        int expected = 24;
        int result = Bloco3Ex1.getResultado(res, num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getResultadoNum8() {
        int res=1, num=8;

        int expected = 40320;
        int result = Bloco3Ex1.getResultado(res, num);
        assertEquals(expected, result, 0.01);
    }
}