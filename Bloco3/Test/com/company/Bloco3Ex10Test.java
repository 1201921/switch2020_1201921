package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex10Test {

    @Test
    void menorNumDoProdutoAcumuladoSupAoNumUtilizadorTest1() {
        double[] numerosPositivos = {12, 7.5, 20.2, 14, 8, 5.5, 33};
        double numUtilizador = 300;

        double expected = 7.5;
        double result = Bloco3Ex10.maiorNumDoProdutoAcumuladoSupAoNumUtilizador(numUtilizador, numerosPositivos);
    }

    @Test
    void menorNumDoProdutoAcumuladoSupAoNumUtilizadorTest2() {
        double[] numerosPositivos = {12, 7.5, 20.2, 14, 8, 5.5, 33};
        double numUtilizador = 2000000;

        double expected = 5.5;
        double result = Bloco3Ex10.maiorNumDoProdutoAcumuladoSupAoNumUtilizador(numUtilizador, numerosPositivos);
    }
}

