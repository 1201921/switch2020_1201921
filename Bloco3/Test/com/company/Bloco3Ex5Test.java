package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex5Test {

    @Test
    void somaNumParesTest1() {
        int numeros [] = {2, 3, 4, 5, 6, 7, 8};

        int expected = 20;
        int result = Bloco3Ex5.somaNumPares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaNumParesTest2() {
        int numeros [] = {14, 21, 8, 17, 15};

        int expected = 22;
        int result = Bloco3Ex5.somaNumPares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void quantidadeParesTest1() {
        int numeros [] = {14, 21, 8, 17, 15};

        int expect = 2;
        int result = Bloco3Ex5.quantidadePares(numeros);
    }

    @Test
    void quantidadeParesTest2() {
        int numeros [] = {2, 3, 4, 5, 6, 7, 8};

        int expect = 4;
        int result = Bloco3Ex5.quantidadePares(numeros);
    }

    @Test
    void somaNumImparesTest1() {
        int numeros [] = {2, 3, 4, 5, 6, 7, 8};

        int expected = 15;
        int result = Bloco3Ex5.somaNumImpares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaNumImparesTest2() {
        int numeros [] = {14, 21, 8, 17, 15};

        int expected = 53;
        int result = Bloco3Ex5.somaNumImpares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void quantidadeImparesTest1() {
        int numeros [] = {14, 21, 8, 17, 15};

        int expected = 3;
        int result = Bloco3Ex5.quantidadeImpares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void quantidadeImparesTest2() {
        int numeros [] = {5, 3, 4, 5, 6, 7, 8};

        int expected = 4;
        int result = Bloco3Ex5.quantidadeImpares(numeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaMultiplosTest1() {
        int numeros[] = {4, 16, 3, -17, 24};
        int dadoNum1 = 4;

        int expected = 44;
        int result = Bloco3Ex5.somaMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaMultiplosTest2() {
        int numeros[] = {4, 16, 3, -17, 24};
        int dadoNum1 = 3;

        int expected = 27;
        int result = Bloco3Ex5.somaMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void produtoMultiplosTest1() {
        int numeros[] = {4, 16, 3, -17, 24};
        int dadoNum1 = 3;

        int expected = 72;
        int result = Bloco3Ex5.produtoMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void produtoMultiplosTest2() {
        int numeros[] = {4, 16, 3, -17, 24};
        int dadoNum1 = 4;

        int expected = 1536;
        int result = Bloco3Ex5.produtoMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaMultiplosTest1() {
        int numeros[] = {4, 16, 3, 17, 24};
        int dadoNum1 = 4;

        double expected = 14.67;
        double result = Bloco3Ex5.mediaMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaMultiplosTest2() {
        int numeros[] = {10, 0, 5, 18, 3, 24, 80, 40}; //0%10=0 logo a quantidade de
        int dadoNum1 = 10;

        double expected = 43.33;
        double result = Bloco3Ex5.mediaMultiplos(numeros, dadoNum1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaMultiplosDadosDoisNumTest1() {
        int numeros [] = {1, 12, 3, 14, 5, 9, 15};
        int dadoNum1 = 2, dadoNum2 = 3;

        double expected = 10.6;
        double result = Bloco3Ex5.mediaMultiplosDadosDoisNum(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaMultiplosDadosDoisNumTest2() {
        int numeros [] = {15, 6, 18, 5, 7, 17, 21, 30};
        int dadoNum1 = 5, dadoNum2 = 6;

        double expected = 14.8;
        double result = Bloco3Ex5.mediaMultiplosDadosDoisNum(numeros, dadoNum1, dadoNum2);
        assertEquals(expected, result, 0.01);
    }
}