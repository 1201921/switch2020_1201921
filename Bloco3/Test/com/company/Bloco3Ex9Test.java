package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex9Test {

    @Test
    void calculoSalarioMensal() {

        String expected = "O salário mensal do funcionário é 1300.0€.";
        String result = Bloco3Ex9.calculoSalarioMensal(1000, 15);
        assertEquals(expected, result);
    }

    @Test
    void mediaSalarios() {

        double expected = 1000.0;
        double result = Bloco3Ex9.mediaSalarios(10000, 10);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void contarFuncionários() {
        char [] funcionarios = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};

        int expected = 10;
        int result = Bloco3Ex9.contarFuncionários(funcionarios);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaSalariosBase() {
        double [] salarioBase2 = {1000, 788.5, 987, 1234.5, 700};

        double expected = 4710.0;
        double result = Bloco3Ex9.somaSalariosBase(salarioBase2);
        assertEquals(expected, result, 0.01);
    }
}