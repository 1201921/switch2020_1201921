package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex13Test {

    @Test
    void classificacaoProdutoTest1() {
        int código = 1;

        String expected = "Alimento não perecível";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }

    @Test
    void classificacaoProdutoTest2() {
        int código = 2;

        String expected = "Alimento perecível";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }

    @Test
    void classificacaoProdutoTest3() {
        int código = 5;

        String expected = "Vestuário";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }

    @Test
    void classificacaoProdutoTest4() {
        int código = 7;

        String expected = "Higiene pessoal";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }

    @Test
    void classificacaoProdutoTest5() {
        int código = 10;

        String expected = "Limpeza e utensílios domésticos";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }

    @Test
    void classificacaoProdutoTest6() {
        int código = 20;

        String expected = "Código inválido";
        String result = Bloco3Ex13.classificacaoProduto(código);
        assertEquals(expected, result);
    }
}