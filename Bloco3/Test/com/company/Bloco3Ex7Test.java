package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex7Test {

    @Test
    void verificarCapicuaTest1() {
        long a = 7887L;

        boolean expected = true;
        boolean result = Bloco3Ex7.verificarCapicua(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarCapicuaTest2() {
        long a = 7845895687L;

        boolean expected = false;
        boolean result = Bloco3Ex7.verificarCapicua(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarCapicuaTest3() {
        long a = 78459895487L;

        boolean expected = true;
        boolean result = Bloco3Ex7.verificarCapicua(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumAmstrongTest1() {
        long a = 153;

        boolean expected = true;
        boolean result = Bloco3Ex7.verificarNumAmstrong(a);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumAmstrongTest2() {
        long a = 819;

        boolean expected = false;
        boolean result = Bloco3Ex7.verificarNumAmstrong(a);
        assertEquals(expected, result);
    }

    @Test
    void primeiraCapicuaNumIntervaloTest1() {
        int expected = 11;
        int result = Bloco3Ex7.primeiraCapicuaNumIntervalo(10, 20);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void primeiraCapicuaNumIntervaloTest2() {
        int expected = 101;
        int result = Bloco3Ex7.primeiraCapicuaNumIntervalo(100, 200);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void maiorCapicuaNumIntervaloTest1() {
        int expected = 999;
        int result = Bloco3Ex7.maiorCapicuaNumIntervalo(500, 1000);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void maiorCapicuaNumIntervaloTest2() {
        int expected = 494;
        int result = Bloco3Ex7.maiorCapicuaNumIntervalo(400, 500);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void numCapicuasNumIntervaloTest1() {
        int expected = 5;
        int result = Bloco3Ex7.numCapicuasNumIntervalo(10, 60);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void numCapicuasNumIntervaloTest2() {
        int expected = 20;
        int result = Bloco3Ex7.numCapicuasNumIntervalo(10, 202);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void primeiroAmstrongNumIntervaloTest1() {
        int expected = 370;
        int result = Bloco3Ex7.primeiroAmstrongNumIntervalo(155, 450);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void primeiroAmstrongNumIntervaloTest2() {
        int expected = 153;
        int result = Bloco3Ex7.primeiroAmstrongNumIntervalo(2, 200);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void primeiroAmstrongNumIntervaloTest3() {
        int expected = 370;
        int result = Bloco3Ex7.primeiroAmstrongNumIntervalo(400, 200);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void numAmstrongsNumIntervaloTest1() {
        int expected = 5;
        int result = Bloco3Ex7.numAmstrongsNumIntervalo(1, 1000);
        assertEquals(expected, result, 0.01);
    }
}