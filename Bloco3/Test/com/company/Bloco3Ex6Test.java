package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Bloco3Ex6Test {

    @Test
    void algarismosNumTest1() {
        long a = 3435849751235487L;

        int expected = 16;
        int result = Bloco3Ex6.algarismosNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void algarismosNumTest2() {
        long a = 478541236548987456L;

        int expected = 18;
        int result = Bloco3Ex6.algarismosNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void algarismosParNumTest1() {
        long a = 3435849751235487L;

        int expected = 6;
        int result = Bloco3Ex6.algarismosParNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void algarismosParNumTest2() {
        long a = 4785412365489874560L;

        int expected = 10;
        int result = Bloco3Ex6.algarismosParNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void algarismosImparNumTest1() {
        long a = 734358497512350L;

        int expected = 10;
        int result = Bloco3Ex6.algarismosImparNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void algarismosImparNumTest2() {
        long a = 47854123654898745L;

        int expected = 7;
        int result = Bloco3Ex6.algarismosImparNum(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaAlgarismosDeUmNumeroTest1() {
        long a = 475896214574L;

        int expected = 62;
        int result = Bloco3Ex6.somaAlgarismosDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaAlgarismosDeUmNumeroTest2() {
        long a = 4586512485654258L;

        int expected = 78;
        int result = Bloco3Ex6.somaAlgarismosDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }


    @Test
    void somaParesDeUmNumeroTest1() {
        long a = 78459012458045L;//8 4 0 2 4 8 0 4

        int expected = 30;
        int result = Bloco3Ex6.somaParesDeUmNumero(a);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void somaParesDeUmNumeroTest2() {
        long a = 14965785423658L; //4 6 8 4 2 6 8

        int expected = 38;
        int result = Bloco3Ex6.somaParesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaImparesDeUmNumeroTest1() {
        long a = 12543879823920573L; //1 5 3 7 9 3 9 5 7 3

        int expected = 52;
        int result = Bloco3Ex6.somaImparesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void somaImparesDeUmNumeroTest2() {
        long a = 34857513264859523L; //3 5 7 5 1 3 5 9 3

        int expected = 46;
        int result = Bloco3Ex6.somaImparesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosDeUmNumeroTest1() {
        long a = 4444444444444444444L;

        double expected = 4.0;
        double result = Bloco3Ex6.mediaAlgarismosDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosDeUmNumeroTest2() {
        long a = 4589634848745411545L;

        double expected = 5.0;
        double result = Bloco3Ex6.mediaAlgarismosDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosParesDeUmNumeroTest1() {
        long a = 4444444444444444L;

        double expected = 4.0;
        double result = Bloco3Ex6.mediaAlgarismosParesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosParesDeUmNumeroTest2() {
        long a = 451265489875L; //4 2 6 4 8 8

        double expected = 5.33;
        double result = Bloco3Ex6.mediaAlgarismosParesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosImparesDeUmNumeroTest1() {
        long a = 451265489875L; //5 1 5 9 7 5

        double expected = 5.33;
        double result = Bloco3Ex6.mediaAlgarismosImparesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaAlgarismosImparesDeUmNumero() {
        long a = 12354879653215L; //1 3 5 7 9 5 3 1 5

        double expected = 4.33;
        double result = Bloco3Ex6.mediaAlgarismosImparesDeUmNumero(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void numeroOrdemInversaTest1() {
        long a = 4587985L;

        long expected = 5897854L;
        long result = Bloco3Ex6.numeroOrdemInversa(a);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void numeroOrdemInversaTest2() {
        long a = 7859512450L;

        long expected = 542159587L;
        long result = Bloco3Ex6.numeroOrdemInversa(a);
        assertEquals(expected, result, 0.01);
    }
}