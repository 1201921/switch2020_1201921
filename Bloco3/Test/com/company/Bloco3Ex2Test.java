package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex2Test {

    @Test
    void mediaNegNumPosTest1() {
        int numAlunos = 5;
        double[] notas = {14.5, 7 , 12.9, 9.5, 5};

        String expected = "Percentagem de positivas: 40,00. Média de negativas: 7,17";
        String result = Bloco3Ex2.mediaNegNumPos(numAlunos, notas);
        assertEquals(expected, result);
    }

    @Test
    void mediaNegNumPosTest2() {
        int numAlunos = -5;
        double[] notas = {14.5, 7 , 12.9, 9.5, 5};

        String expected = "Nº de alunos inválido";
        String result = Bloco3Ex2.mediaNegNumPos(numAlunos, notas);
        assertEquals(expected, result);
    }

    @Test
    void mediaNegNumPosTest3() {
        int numAlunos = 5;
        double[] notas = {20, -7, 12.9, 9.5, 5};

        String expected = "Dados Inválidos";
        String result = Bloco3Ex2.mediaNegNumPos(numAlunos, notas);
        assertEquals(expected, result);
    }

    @Test
    void mediaNegNumPosTest4() {
        int numAlunos = 5;
        double[] notas = {20, 33, 12.9, 9.5, 5};

        String expected = "Dados Inválidos";
        String result = Bloco3Ex2.mediaNegNumPos(numAlunos, notas);
        assertEquals(expected, result);
    }
}