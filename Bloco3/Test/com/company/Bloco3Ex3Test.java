package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Ex3Test {

    @Test
    void percParesTest1() {
        int numeros [] = {2,5,7,10,15,-3};
        int numNumeros=6;

        double expected = 33.33;
        double result = Bloco3Ex3.percPares(numeros, numNumeros );
        assertEquals(expected, result, 0.01);
    }

    @Test
    void percParesTest2() {
        int numeros [] = {5, 12, 8, 17, 14, 2, 9, 11, 20, 21, -8};
        int numNumeros=11;

        double expected = 54.55;
        double result = Bloco3Ex3.percPares(numeros, numNumeros );
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaImparesTest1() {
        int numeros [] = {2,5,7,10,15,-3};
        int numNumeros=6;

        double expected = 6.0;
        double result = Bloco3Ex3.MediaImpares(numeros, numNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaImparesTest2() {
        int numeros[] = {5, 12, 8, 17, 14, 2, 9, 11, 20, 21, -8};
        int numNumeros = 11;

        double expected = 12.60;
        double result = Bloco3Ex3.MediaImpares(numeros, numNumeros);
        assertEquals(expected, result, 0.01);
    }
}