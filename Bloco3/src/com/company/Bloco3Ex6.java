package com.company;

public class Bloco3Ex6 {
    public static void main(String[] args) {
        long a;

        }


    //a)
    public static int algarismosNum(long a) {
        int qntAlgarismos = 1;

        do {
            a = a / 10;
            qntAlgarismos = qntAlgarismos + 1;
        } while (a > 9);

        return qntAlgarismos;
    }

    //b)
    public static int algarismosParNum(long a) {
        int qntAlgarismosPar = 0;

        do {
            a = a / 10;
            if (a % 2 == 0) {
                qntAlgarismosPar = qntAlgarismosPar + 1;
            }
        } while (a > 9);

        return qntAlgarismosPar;
    }

    //c)
    public static int algarismosImparNum(long a) {
        int qntAlgarismosImpar = 0;

        do {
            a = a / 10;
            if (a % 2 != 0) {
                qntAlgarismosImpar = qntAlgarismosImpar + 1;
            }
        } while (a > 9);

        return qntAlgarismosImpar;
    }

    //d)
    public static int somaAlgarismosDeUmNumero(long a) {
        int somaAlgarismos = 0;
        while(a>0){
            long algarismo = a%10;
            a=a/10;
            somaAlgarismos=somaAlgarismos+(int)algarismo;
        }
        return somaAlgarismos;
    }

    //e)
    public static int somaParesDeUmNumero (long a){
        int somaPares=0;
        long algarismo;
        while (a>0) {
            if (a % 2 == 0) {
                algarismo = a % 10;
            }
            else algarismo=0;
            somaPares = somaPares + (int)algarismo;
            a = a / 10;
        }
        return somaPares;
    }

    //f)
    public static int somaImparesDeUmNumero (long a){
        int somaImpares=0;
        long algarismo;
        while (a>0) {
            if (a % 2 != 0) {
                algarismo = a % 10;
            } else algarismo = 0;
            somaImpares = somaImpares + (int) algarismo;
            a = a / 10;
        } return somaImpares;
    }

    //g
    public static double mediaAlgarismosDeUmNumero (long a){
        int somaAlgarismos=0, qntAlgarismos=0;
        double media;
        long algarismo;
        do {
            algarismo=a%10;
            a = a / 10;
            qntAlgarismos = qntAlgarismos + 1;
            somaAlgarismos = somaAlgarismos + (int) algarismo;

        } while (a > 9);
        media=somaAlgarismos/qntAlgarismos;

        return media;
    }

    //h)
    public static double mediaAlgarismosParesDeUmNumero (long a){
        double somaAlgarismosPares=0, qntAlgarismosPares=0;
        double media;
        long algarismo;

        while (a>0) {
            if (a % 2 == 0) {
                algarismo = a % 10;
                qntAlgarismosPares=qntAlgarismosPares+1;
                somaAlgarismosPares = somaAlgarismosPares + (int)algarismo;
            }
            a = a / 10;
        }

        media=somaAlgarismosPares/qntAlgarismosPares;

        return media;
    }

    //i)
    public static double mediaAlgarismosImparesDeUmNumero (long a){
        double somaAlgarismosIpmares=0, qntAlgarismosIpmares=0;
        double media;
        long algarismo;

        while (a>0) {
            if (a % 2 != 0) {
                algarismo = a % 10;
                qntAlgarismosIpmares=qntAlgarismosIpmares+1;
                somaAlgarismosIpmares = somaAlgarismosIpmares + (int)algarismo;
            }
            a = a / 10;
        }

        media=somaAlgarismosIpmares/qntAlgarismosIpmares;

        return media;
    }

    //h)
    public static long numeroOrdemInversa (long a){
        long num = a, inv=0; //num = long a; inv = nº ordem inversa

        /*while (num!=0){
            inv=inv*10;     //dá a ordem de cada nº (un, dez, cent,...)
            inv=inv+num%10; //o resto do nº original atribui o valor às ordens
            num=num/10;     //diminui o nº de ordens do nº original (... cent >> dez >> un)
        }*/

        for (num=a; num != 0; num=num/10){
            inv=inv*10;
            inv=inv+num%10;

        }
        return inv;
    }

}

