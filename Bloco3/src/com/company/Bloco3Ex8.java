package com.company;

public class Bloco3Ex8 {
    public static void main(String[] args) {
        double numUtilizador;
        double [] numerosPositivos;

    }



    public static double menorNumDaSomaAcumuladaSupAoNumUtilizador (double numUtilizador, double [] numerosPositivos){
        double soma = 0;
        double menorNum=numerosPositivos[0];


        for (int i = 0; i < numerosPositivos.length && soma <= numUtilizador; i++) {
            soma = soma + numerosPositivos[i];

            if ( numerosPositivos[i] < menorNum) {
                menorNum = numerosPositivos[i];
            }
        }
        return menorNum;
    }
}
