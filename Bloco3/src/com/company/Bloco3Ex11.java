package com.company;
import java.util.Scanner;
public class Bloco3Ex11 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int n;
        System.out.println("Insira um nº inteiro de 0 a 20");
        n=ler.nextInt();
        if (n < 1 || n > 20) {
            System.out.println("Nº inválido.");
        } else {
            System.out.println("Existem " + getNumSomas(n) + " maneiras de obter " + n + ".");
        }
    }

    public static int getNumSomas (int n){
        int x, y;
        int contagem=0;

        for (x=0; x<10; x++){
            for (y=10; y>=x; y--){
                if (n == x + y){
                    contagem +=1;
                    // se quisesse indicar quais as conjugações fazia p.e. resultado=resultado + x "+" + y + " "; e teria
                    //que retornar uma String "Existem " + contagem + " maneiras " + resultado
                }
            }
        } return contagem;
    }
}
