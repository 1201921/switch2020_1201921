package com.company;

public class Bloco3Ex2 {
    public static void main(String[] args) {
        int numAlunos;
        double notas [];
    }

    public static String mediaNegNumPos (int numAlunos, double notas []){
        double numNotasPos=0, numNotasNeg = 0, somaNotaNeg = 0, mediaNotasNeg,
                percPos;
        String mediaNegNumPos;
        if (numAlunos<=0) return "Nº de alunos inválido";
        else {
            for (int i=0; i<numAlunos;i++){
                double nota = notas [i];
                if (nota>=10 && nota<=20) numNotasPos = numNotasPos+1;
                else if (nota<10 && nota>=0){
                    numNotasNeg = numNotasNeg + 1;
                    somaNotaNeg = somaNotaNeg + notas[i];
                }
                else return "Dados Inválidos";
            }
        }
        percPos = numNotasPos/numAlunos*100;
        mediaNotasNeg = somaNotaNeg/numNotasNeg;

        mediaNegNumPos = "Percentagem de positivas: " + String.format("%.2f", percPos) + ". Média de negativas: " + String.format("%.2f", mediaNotasNeg);

        return mediaNegNumPos;
    }

}
