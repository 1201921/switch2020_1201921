package com.company;
import java.util.Scanner;
public class Bloco3Ex9 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        double salarioBase;
        int horasExtra;
        System.out.println("Introduza o salário do funcionário:");
        if (ler.hasNextDouble() == true) {
            salarioBase = ler.nextDouble();
            System.out.println("Introduza o nº de horas extra:");
            if (ler.hasNextInt() == true) {
                horasExtra = ler.nextInt();
                if (horasExtra < 0) {
                    System.out.println("Nº de horas extra inválido. Introduza o nº de horas extra novamente:");
                    horasExtra = ler.nextInt();
                }
            } else {
                System.out.println("Nº horas extra inválido");
            }
        } else {
            System.out.println("Salário base inválido");
        }

        char [] funcionários;
        double [] salarioBase2;

    }

    public static String calculoSalarioMensal (double salarioBase, int horasExtra){
        if (horasExtra<0) horasExtra=Math.abs(horasExtra); //resolve valor horaEstra negativo nos testes
        double salarioMensal = salarioBase + (salarioBase*horasExtra*0.02);
        return ("O salário mensal do funcionário é " + salarioMensal + "€.");
    }

    public static int contarFuncionários (char [] funcionários){
        int contagem=0;
        for (int i=0; i < (int) funcionários.length; i++){
            contagem = i+1;
        }
        return contagem;
    }

    public static double somaSalariosBase (double [] salarioBase2){
        double somaSalarios=0;
        for (int i = 0; i < salarioBase2.length;i++){
            somaSalarios=somaSalarios+salarioBase2[i];
        }
        return somaSalarios;
    }

    public static double mediaSalarios (double somaSalariosBase, int contarFuncionários){
        double media = somaSalariosBase/ contarFuncionários;
        return media;
    }
}
