package com.company;

public class Bloco3ex4 {
    public static void main(String[] args) {

        int numeros []; //a)
        int dadoNum1;   //b), d) e e)
        int dadoNum2;   // d) e e)
    }
    //a)
    public static int multiplos3 (int numeros[]){
        int quantMultiplos3=0;
        for (int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero % 3 == 0) quantMultiplos3 = quantMultiplos3 + 1;
        }
        return quantMultiplos3;
    }

    //b)
    public static int multiplosDadoNum (int numeros[], int dadoNum1){
        int quantMultiplos=0;

        for (int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero % dadoNum1 == 0) quantMultiplos = quantMultiplos + 1;
        }
        return quantMultiplos;
    }

    //c))
    public static int multiplos3e5 (int numeros[]){
        int quantMultiplos3e5=0;
        for (int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero % 3 == 0 && numero % 5 == 0) {
                quantMultiplos3e5 = quantMultiplos3e5 + 1;
            }
        }
        return quantMultiplos3e5;
    }

    //d)
    public static int multiplosDoisNumeros (int numeros[], int dadoNum1, int dadoNum2){
        int quantMultiplos=0;

        for (int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero % dadoNum1 == 0 && numero % dadoNum2 == 0) quantMultiplos = quantMultiplos + 1;
        }
        return quantMultiplos;
    }

    //e)
    public static int somaMultiplos (int numeros[], int dadoNum1, int dadoNum2){
        int quantMultiplos=0;
        int somaMultiplos=0;

        for (int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero % dadoNum1 == 0 && numero % dadoNum2 == 0){
                quantMultiplos = quantMultiplos + 1;
                somaMultiplos = somaMultiplos + numeros[i];
            }
        }
        return somaMultiplos;
    }

}
