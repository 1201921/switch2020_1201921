package com.company;
import java.util.Scanner;
public class Bloco3Ex7 {
    public static void main(String[] args) {
        Scanner ler = new Scanner((System.in));
        

        long a;
        int limMinIntervalo, limMaxIntervalo;




    }

    //a)
    public static boolean verificarCapicua (long a){
        boolean res = true; //resultado
        long num;
        long inv = 0;

        for (num=a; num != 0; num=num/10){
            inv=inv*10;
            inv=inv+num%10;
        }
        if (inv==a) {
            return res = true;
        }
        else return res = false;
    }

    //b)
    public static boolean verificarNumAmstrong (long a){
        boolean nArmstrong;
        long num = 0, b=a;
        while (b>0){
            long algarismo = b%10;
            num = num + (algarismo*algarismo*algarismo);
            b=b/10;
        }
        if (a==num){
            return nArmstrong = true;
        }
        else return nArmstrong = false;
    }

    //c
    public static int primeiraCapicuaNumIntervalo (int limMinIntervalo, int limMaxIntervalo) {
        int numero;
        int inverso = 0;

        for (numero = limMinIntervalo; numero <= limMaxIntervalo; numero++) {
            int num = numero;
            inverso = 0;
            while (num != 0) {
                inverso = inverso * 10;
                inverso = inverso + num % 10;
                num = num / 10;

            }
            if (inverso == numero) break;
        }
        return inverso;
    }

    //d
    public static int maiorCapicuaNumIntervalo (int limMinIntervalo, int limMaxIntervalo){
        if (limMaxIntervalo<limMinIntervalo){
            int temp = limMinIntervalo;
            limMinIntervalo = limMaxIntervalo;  // previne e resolve valor inicio > valor fim
            limMaxIntervalo = temp;
        }
        int numero;
        int inverso = 0;

        for (numero = limMaxIntervalo; numero >= limMinIntervalo; numero--) {
            int num = numero;
            inverso = 0;
            while (num != 0) {
                inverso = inverso * 10;
                inverso = inverso + num % 10;
                num = num / 10;

            }
            if (inverso == numero) {
                break;
            }
        }
        return inverso;
    }

    //e))
    public static int numCapicuasNumIntervalo (int limMinIntervalo, int limMaxIntervalo){
        if (limMaxIntervalo<limMinIntervalo){
            int temp = limMinIntervalo;
            limMinIntervalo = limMaxIntervalo;
            limMaxIntervalo = temp;
        }

        int numero;
        int inverso = 0;
        int qntd=0;

        for (numero = limMinIntervalo; numero <= limMaxIntervalo; numero++) {
            int num = numero;
            inverso = 0;
            while (num != 0) {
                inverso = inverso * 10;
                inverso = inverso + num % 10;
                num = num / 10;

            }
            if (inverso == numero) qntd=qntd+1;
        }
        return qntd;
    }

    //f)
    public static int primeiroAmstrongNumIntervalo (int limMinIntervalo, int limMaxIntervalo){
        if (limMaxIntervalo<limMinIntervalo){
            int temp = limMinIntervalo;
            limMinIntervalo = limMaxIntervalo;
            limMaxIntervalo = temp;
        }

        int original;
        int num = 0;
        for (original = limMinIntervalo; original <= limMaxIntervalo; original++){
            int i = original;
            num=0;
            while (i!=0) {
                int algarismo = i % 10;
                num = num + (algarismo * algarismo * algarismo);
                i = i / 10;
            }if (original == num) break;
        }
        return original;
    }

    //g)
    public static int numAmstrongsNumIntervalo (int limMinIntervalo, int limMaxIntervalo){
        if (limMaxIntervalo<limMinIntervalo){
            int temp = limMinIntervalo;
            limMinIntervalo = limMaxIntervalo;
            limMaxIntervalo = temp;
        }
        int original, qntd=0;
        int num = 0;
        for (original = limMinIntervalo; original <= limMaxIntervalo; original++){
            int i = original;
            num=0;
            while (i!=0) {
                int algarismo = i % 10;
                num = num + (algarismo * algarismo * algarismo);
                i = i / 10;
            }if (original == num) qntd = qntd + 1;
        }
        return qntd;
    }
}
