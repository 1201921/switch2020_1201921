package com.company;

public class Bloco3Ex19 {
    public static void main(String[] args) {
        int [] numeros = {0, 7, 4, 8, 6, 5, 5, 1, 2};
        String localizacao;

        for (int i=0; i<numeros.length; i++){
            if (numeros[i]%2 == 0){
                localizacao = "\t" + numeros[i] + "\n";
            } else localizacao = numeros[i] + "\n";
            System.out.print(localizacao);
        }
    }
}
