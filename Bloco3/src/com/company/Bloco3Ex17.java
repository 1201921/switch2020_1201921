package com.company;

import java.util.Scanner;
public class Bloco3Ex17 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        double pesoAnimal=0;
        int qntRacao;

        do {
            System.out.println("Qual o peso do animal? (em kg)");
            pesoAnimal = ler.nextDouble();

            System.out.println("Qual a quantidade de ração diária? (em Gramas)");
            qntRacao = ler.nextInt();

            System.out.println(verificarAdequacaoRacao(pesoAnimal, qntRacao));

            for (int i=2; i>0;i++) {
                System.out.println("Qual o peso do " + i +"º animal? (em kg)");
                pesoAnimal = ler.nextDouble();

                if (pesoAnimal < 0) {
                    System.out.println("Questionário terminado.");
                    break;}

                    System.out.println("Qual a quantidade de ração diária? (em Gramas)");
                    qntRacao = ler.nextInt();

                    System.out.println(verificarAdequacaoRacao(pesoAnimal, qntRacao));
            }
        } while (pesoAnimal>=0);
    }

    public static String verificarAdequacaoRacao(double pesoAnimal, int qntRacao) {
        if (pesoAnimal <= 10 && qntRacao == 100) {
            return "Quantidade de ração adequada.\n";
        } else if (pesoAnimal > 10 && pesoAnimal <= 25 && qntRacao == 250) {
            return "Quantidade de ração adequada.\n";
        } else if (pesoAnimal > 25 && pesoAnimal <= 45 && qntRacao == 300) {
            return "Quantidade de ração adequada.\n";
        } else if (pesoAnimal > 45 && qntRacao == 500) {
            return "Quantidade de ração adequada.\n";
        } else return "Quantidade de ração não adequada.\n";
    }
}