package com.company;

public class Bloco3Ex10 {
    public static void main(String[] args) {
        double numUtilizador;
        double [] numerosPositivos;

    }

    public static double maiorNumDoProdutoAcumuladoSupAoNumUtilizador (double numUtilizador, double [] numerosPositivos){
        double produto = 0;
        double maiorNum=numerosPositivos[0];


        for (int i = 0; i < numerosPositivos.length && produto <= numUtilizador; i++) {
            produto = produto * numerosPositivos[i];

            if ( numerosPositivos[i] < maiorNum) {
                maiorNum = numerosPositivos[i];
            }
        }
        return maiorNum;
    }
}
