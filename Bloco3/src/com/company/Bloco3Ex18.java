package com.company;
import java.util.Scanner;
public class Bloco3Ex18 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int numCC, numVerificacao;

        System.out.println("Indique o seu nº de CC.");
        numCC = ler.nextInt();

        System.out.println("\nIndique o algarismo de verificação.");
        numVerificacao=ler.nextInt();

        System.out.println("\n" + validarNumCC(numCC,numVerificacao));

    }

    public static String validarNumCC (int numCC, int numVerificacao){
        int numCompleto = numCC*10+numVerificacao;
        String validacaoCC="";
        int somaPonderada=0;

        for (int i = 1; i<=9; i++){
            somaPonderada= somaPonderada + numCompleto%10*i;
            numCompleto=numCompleto/10;
        }

        if (somaPonderada%11==0){
            validacaoCC = "Dados de CC/BI corretos";
        } else{
            validacaoCC = "Dados de CC/BI incorretos";
        }
        return validacaoCC;
    }
}
