package com.company;

public class Bloco3Ex3 {
    public static void main(String[] args) {
        int numeros[] = {2, 5, 7, 10, 15, -3};
        int numNumeros = 6;

    }



    public static double percPares (int numeros [], int numNumeros){
        int quantidadePares=0;

        for (int i = 0; i<numNumeros; i++ ) {
            int numero=numeros[i];
            if (numero%2 == 0) quantidadePares = quantidadePares +1;
        }
        double percPar = (double) quantidadePares/numNumeros*100;
        return percPar;
    }

    public static double MediaImpares (int numeros [], int numNumeros){
        int quantidadeImpares=0, somaNumImpares=0;

        for (int i = 0; i<numNumeros; i++ ) {
            int numero=numeros[i];
            if (numero%2 != 0){
                quantidadeImpares = quantidadeImpares + 1;
                somaNumImpares = somaNumImpares + numeros[i];
            }

        }
        double mediaImpares = (double) somaNumImpares/quantidadeImpares;
        return mediaImpares;
    }
}

