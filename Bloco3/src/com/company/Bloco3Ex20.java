package com.company;
import java.util.Scanner;
public class Bloco3Ex20 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        int num;

        System.out.println("Insira um nº inteiro positivo:");
        num=ler.nextInt();
        System.out.println("\n" + getTipoNum(num));
    }

    public static String getTipoNum (int num){
        String tipoNum = "";
        int soma=0;
        for (int i = 1; i < num; i++){
            if (num%i==0){ // se resto zero então i é divisor de num
                soma = soma + i;
            }
            if (num == soma) {
                tipoNum = num + " é um nº Perfeito.";
            } else if (num < soma) {
                tipoNum = num + " é um nº Abundante.";
            } else tipoNum = num + " é um nº Reduzido.";
        }
        return tipoNum;
    }
}
