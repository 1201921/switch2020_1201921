package com.company;
import java.util.Scanner;
public class Bloco3Ex13 {
    public static void main(String[] args){
        Scanner ler = new Scanner (System.in);
        int código;
        do {
            System.out.println("Insira o código do produto:");
            código = ler.nextInt();

            System.out.println(classificacaoProduto(código));
            for (int i=1; i>=1; i++){
                System.out.println("Insira o próximo código:");
                código= ler.nextInt();
                if (código==0){
                    System.out.println("Tarefa terminada.");
                    break;
                }
                System.out.println(classificacaoProduto(código));
            }

        }while (código!=0);
    }

    public static String classificacaoProduto (int código){
        String x = "";

        if (código == 1) x = "Alimento não perecível\n";
        else if (código >= 2 && código <= 4) x = "Alimento perecível\n";
        else if (código >= 5 && código <= 6) x = "Vestuário\n";
        else if (código == 7) x = "Higiene pessoal\n";
        else if (código >= 8 && código <= 15) x = "Limpeza e utensílios domésticos\n";
        else x = "Código inválido\n";

        return x;
    }
}

