package com.company;

public class Bloco3Ex5 {
    public static void main(String[] args) {
        int numeros [];
        int dadoNum1, dadoNum2;
    }

    //a)
    public static int somaNumPares (int numeros[]){
        int somaNumPares=0;
        for(int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero%2==0){
                somaNumPares= somaNumPares + numeros[i];
            }
        }
        return somaNumPares;
    }

    //b)
    public static int quantidadePares (int numeros[]){
        int quantidadePares=0;
        for(int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero%2==0){
                quantidadePares = quantidadePares + 1;
            }
        }
        return quantidadePares;
    }

    //c)
    public static int somaNumImpares (int numeros[]){
        int somaNumImpares=0;
        for(int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero%2!=0){
                somaNumImpares= somaNumImpares + numeros[i];
            }
        }
        return somaNumImpares;
    }

    //d)
    public static int quantidadeImpares (int numeros[]){
        int quantidadeImpares=0;
        for(int i=0; i<numeros.length; i++) {
            int numero = numeros[i];
            if (numero%2!=0){
                quantidadeImpares = quantidadeImpares + 1;
            }
        }
        return quantidadeImpares;
    }

    //e)
    public static int somaMultiplos (int numeros [], int dadoNum1){
        int somaMultiplos=0;
        int numero;

        for(int i=0; i<numeros.length; i++){
            numero = numeros[i];
            if (numero%dadoNum1==0){
                somaMultiplos = somaMultiplos + numeros[i];
            }
        } return somaMultiplos;
    }

    //f)
    public static int produtoMultiplos (int numeros [], int dadoNum1){
        int produtoMultiplos=1;
        int numero;

        for(int i=0; i<numeros.length; i++){
            numero = numeros[i];
            if (numero%dadoNum1==0){
                produtoMultiplos = produtoMultiplos * numeros[i];
            }
        } return produtoMultiplos;
    }

    //g)
    public static double mediaMultiplos (int numeros[], int dadoNum1){
        int quantidadeMultiplos=0;
        int somaMultiplos=0;
        int numero;

        for(int i=0; i<numeros.length; i++){
            numero = numeros[i];
            if (numero>0 && numero%dadoNum1==0){
                quantidadeMultiplos = quantidadeMultiplos + 1;
                somaMultiplos = somaMultiplos + numeros[i];
            }
        }
        double mediaMultiplos = (double) somaMultiplos/quantidadeMultiplos;
        return mediaMultiplos;
    }

    //h)
    public static double mediaMultiplosDadosDoisNum (int numeros [], int dadoNum1, int dadoNum2){
        int quantidadeMultiplos=0;
        int somaMultiplos=0;
        int numero;

        for(int i=0; i<numeros.length; i++){
            numero = numeros[i];
            if (numero>0){
                if(numero%dadoNum1==0 || numero%dadoNum2==0){
                    quantidadeMultiplos = quantidadeMultiplos + 1;
                    somaMultiplos = somaMultiplos + numeros[i];
                }
            }
        }
        double mediaMultiplos = (double) somaMultiplos/quantidadeMultiplos;
        return mediaMultiplos;

    }
}
