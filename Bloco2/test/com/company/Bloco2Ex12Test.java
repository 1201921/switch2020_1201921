package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex12Test {

    @Test
    void notificaçãoTeste1() {
        double indPoluicao = 0;

        String expected = "Podem continuar as atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste2() {
        double indPoluicao = 0.24;

        String expected = "Podem continuar as atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste3() {
        double indPoluicao = 0.31;

        String expected = "As industrias do 1º grupo têm de suspender atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste4() {
        double indPoluicao = 0.4;

        String expected = "As industrias do 1º grupo têm de suspender atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste5() {
        double indPoluicao = 0.42;

        String expected = "As industrias do 1º e 2º grupos têm de suspender atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste6() {
        double indPoluicao = 0.5;

        String expected = "As industrias do 1º e 2º grupos têm de suspender atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }

    @Test
    void notificaçãoTeste7() {
        double indPoluicao = 0.55;

        String expected = "Todas as industrias têm de suspender atividades";
        String result = Bloco2Ex12.notificação(indPoluicao);
        assertEquals(expected, result);
    }
}