package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex19Test {

    @Test
    void horasExtras36Horas() {
        int hSemanais = 36;
        double hora = 7.5;

        double expected = 270;
        double result = Bloco2Ex19.horasExtras(hSemanais, hora);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void horasExtras38Horas() {
        int hSemanais = 38;
        double hora = 7.5;

        double expected = 290;
        double result = Bloco2Ex19.horasExtras(hSemanais, hora);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void horasExtras42Horas() {
        int hSemanais = 42;
        double hora = 7.5;

        double expected = 335;
        double result = Bloco2Ex19.horasExtras(hSemanais, hora);
        assertEquals(expected, result, 0.001);
    }
}