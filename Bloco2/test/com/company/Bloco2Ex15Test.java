package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex15Test {

    @Test
    void tipoTrianguloEquilátero() {
        double lado1=10, lado2=10, lado3=10;

        String expected = "Triangulo Equilátero";
        String result = Bloco2Ex15.tipoTriangulo(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloEscaleno() {
        double lado1=10, lado2=11, lado3=12;

        String expected = "Triangulo Escaleno";
        String result = Bloco2Ex15.tipoTriangulo(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloIsósceles() {
        double lado1=10, lado2=10, lado3=12;

        String expected = "Triangulo Isósceles";
        String result = Bloco2Ex15.tipoTriangulo(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloImpossivel() {
        double lado1=40, lado2=10, lado3=12;

        String expected = "Triangulo impossível";
        String result = Bloco2Ex15.tipoTriangulo(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloMedidasInvalidas() {
        double lado1=-40, lado2=10, lado3=12;

        String expected = "Medidas inválidas";
        String result = Bloco2Ex15.tipoTriangulo(lado1, lado2, lado3);
        assertEquals(expected, result);
    }
}