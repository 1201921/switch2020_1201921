package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex20Test {

    @Test
    void preco2feiraKit1() {
        int diaSemana = 1;
        int tipoKit = 1;
        double kms=20;

        String expected = "O preço do aluguer é de 30€ e 40.0€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void preco5feiraKit2() {
        int diaSemana = 4;
        int tipoKit = 2;
        double kms=35;

        String expected = "O preço do aluguer é de 50€ e 70.0€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void preco6feiraKit3() {
        int diaSemana = 5;
        int tipoKit = 3;
        double kms = 14.3;

        String expected = "O preço do aluguer é de 100€ e 28.6€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoSabadoKit1() {
        int diaSemana = 6;
        int tipoKit = 1;
        double kms = 25;

        String expected = "O preço do aluguer é de 40€ e 50.0€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoDomingoKit2() {
        int diaSemana = 7;
        int tipoKit = 2;
        double kms = 25;

        String expected = "O preço do aluguer é de 70€ e 50.0€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoFeriadoKit3() {
        int diaSemana = 8;
        int tipoKit = 3;
        double kms = 25;

        String expected = "O preço do aluguer é de 140€ e 50.0€ de transporte";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoDiaInvalido1() {
        int diaSemana = 0;
        int tipoKit = 1;
        double kms = 25;

        String expected = "Dia da semana inválido";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoDiaInvalido2() {
        int diaSemana = 10;
        int tipoKit = 1;
        double kms = 25;

        String expected = "Dia da semana inválido";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }

    @Test
    void precoKitInvalido() {
        int diaSemana = 1;
        int tipoKit = 4;
        double kms = 25;

        String expected = "Tipo de Kit inválido";
        String result = Bloco2Ex20.preco(diaSemana,tipoKit, kms);
        assertEquals(expected, result);
    }
}