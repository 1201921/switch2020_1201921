package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex4Test {

    @Test
    void xPositivo() {
        int x = 4;

        int expected = 8;
        int result = Bloco2Ex4.xPositivo(x);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void xNegativo() {
        int x = -5;

        int expected = -5;
        int result = Bloco2Ex4.xNegativo(x);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void xZero() {
        int x = 0;

        int expected = 0;
        int result = Bloco2Ex4.xZero(x);
        assertEquals(expected, result, 0.01);
    }
}