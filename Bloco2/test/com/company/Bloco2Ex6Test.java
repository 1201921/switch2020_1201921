package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex6Test {

    @Test
    void getSegundos() {
        int momentoDia = 55456;

        int expected = 16;
        int result = Bloco2Ex6.getSegundos(momentoDia);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMinutos() {
        int momentoDia = 55456;

        int expected = 24;
        int result = Bloco2Ex6.getMinutos(momentoDia);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getHoras() {
        int momentoDia = 55456;

        int expected = 15;
        int result = Bloco2Ex6.getHoras(momentoDia);
        assertEquals(expected, result, 0.01);
    }
}