package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex7Test {

    @Test
    void getNumPintoresTeste1() {
        double areaEdificio = 90;

        double expected = 1;
        double result = Bloco2Ex7.getNumPintores(areaEdificio);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumPintoresTeste2() {
        double areaEdificio = 215;

        double expected = 2;
        double result = Bloco2Ex7.getNumPintores(areaEdificio);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumPintoresTeste3() {
        double areaEdificio = 845;

        double expected = 3;
        double result = Bloco2Ex7.getNumPintores(areaEdificio);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumPintoresTeste4() {
        double areaEdificio = 1234;

        double expected = 4;
        double result = Bloco2Ex7.getNumPintores(areaEdificio);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTintaGastaTeste1() {
        double areaEdificio = 1000;
        double areaLitro = 0.8;

        double expected = 1250;
        double result = Bloco2Ex7.getTintaGasta(areaEdificio, areaLitro);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTintaGastaTeste2() {
        double areaEdificio = 354;
        double areaLitro = 1.5;

        double expected = 236;
        double result = Bloco2Ex7.getTintaGasta(areaEdificio, areaLitro);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getCustoMaoObraTeste1() {
        double numPintores = 4;
        double salarioDia = 30;
        double diasTrabalho = 15.625;

        double expected = 1875;
        double result = Bloco2Ex7.getCustoMaoObra(numPintores, salarioDia, diasTrabalho);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getCustoMaoObraTeste2() {
        double numPintores = 2;
        double salarioDia = 66;
        double diasTrabalho = 10;

        double expected = 1320;
        double result = Bloco2Ex7.getCustoMaoObra(numPintores, salarioDia, diasTrabalho);
        assertEquals(expected, result, 0.01);
    }
}