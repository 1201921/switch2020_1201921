package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2ex16Test {

    @Test
    void tipoTrianguloImpossivel() {
        double a=0, b=95, c=85;

        String expected = "Nenhum angulo pode ser inferior a 0º";
        String result = Bloco2ex16.tipoTriangulo(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloImpossivel2() {
        double a=45, b=65, c=85;

        String expected = "A soma dos angulos internos de triangulo tem de ser igual a 180º";
        String result = Bloco2ex16.tipoTriangulo(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloRectangulo() {
        double a=90, b=45, c=45;

        String expected = "Triangulo Rectangulo";
        String result = Bloco2ex16.tipoTriangulo(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloObtusangulo() {
        double a=95, b=40, c=45;

        String expected = "Triangulo Obtusangulo";
        String result = Bloco2ex16.tipoTriangulo(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void tipoTrianguloAcutangulo() {
        double a=60, b=65, c=55;

        String expected = "Triangulo Acutangulo";
        String result = Bloco2ex16.tipoTriangulo(a, b, c);
        assertEquals(expected, result);
    }
}