package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex14Test {

    @Test
    void getMediaTeste1() {
        double dia1=158, dia2=269, dia3=147, dia4=245, dia5=485;

        double expected = 419.6272;
        double result = Bloco2Ex14.getMedia(dia1, dia2, dia3, dia4, dia5);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void getMediaTeste2() {
        double dia1=85.64, dia2=78.48, dia3=69.69, dia4=46.54, dia5=47;

        double expected = 105.34123;
        double result = Bloco2Ex14.getMedia(dia1, dia2, dia3, dia4, dia5);
        assertEquals(expected, result, 0.001);
    }
}