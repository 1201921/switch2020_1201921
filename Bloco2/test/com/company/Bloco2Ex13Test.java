package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex13Test {

    @Test
    void custoTotal() {
        double areaGrama = 46, tempoGrama=300.0/3600, tempoArvore = 600.0/3600, tempoArbusto = 400.0/3600;
        int numArvores = 14, numArbustos = 20, maoObra=10,  custoGrama=10, custoArbusto=15, custoArvore=20;

        double expected = 1123.888;
        double result = Bloco2Ex13.custoTotal(maoObra,tempoGrama,tempoArvore,tempoArbusto,custoArvore, custoArbusto, custoGrama, numArvores, numArbustos, areaGrama);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void horasServiço() {
        double areaGrama = 46, tempoGrama=300.0/3600, tempoArvore = 600.0/3600, tempoArbusto = 400.0/3600;
        int numArvores = 14, numArbustos = 20;

        double expected = 8.38;
        double result = Bloco2Ex13.horasServiço(numArvores, numArbustos, areaGrama, tempoArbusto, tempoArvore, tempoGrama);
        assertEquals(expected, result, 0.01);
    }
}