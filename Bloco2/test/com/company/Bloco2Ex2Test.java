package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex2Test {
    @Test
    void getDigito1() {
        int numero=658;

        int expected = 6;
        int result = Bloco2Ex2.getDigito1(numero);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDigito2() {
        int numero=658;

        int expected = 5;
        int result = Bloco2Ex2.getDigito2(numero);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDigito3() {
        int numero=658;

        int expected = 8;
        int result = Bloco2Ex2.getDigito3(numero);
        assertEquals(expected, result, 0.001);
    }
}