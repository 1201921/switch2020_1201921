package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex8Test {

    @Test
    void confirmarMultiploTeste1() {
        int x=50;
        int y=5;

        String expected = "x é multiplo de y";
        String result = Bloco2Ex8.confirmarMultiplo(x,y);
        assertEquals(expected, result);
    }

    @Test
    void confirmarMultiploTeste2() {
        int x=3;
        int y=30;

        String expected = "y é multiplo de x";
        String result = Bloco2Ex8.confirmarMultiplo(x,y);
        assertEquals(expected, result);
    }

    @Test
    void confirmarMultiploTeste3() {
        int x=61;
        int y=6;

        String expected = "x não é multiplo nem dividor de y";
        String result = Bloco2Ex8.confirmarMultiplo(x,y);
        assertEquals(expected, result);
    }
}