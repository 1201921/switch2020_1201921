package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex9Test {

    @Test
    void confirmarCrescenteTrue() {
        int numero  = 456;

        String expected = "O nº é crescente";
        String result = Bloco2Ex9.confirmarCrescente(numero);
        assertEquals(expected, result);

    }

    @Test
    void confirmarCrescenteFalse() {
        int numero  = 265;

        String expected = "O nº não é crescente";
        String result = Bloco2Ex9.confirmarCrescente(numero);
        assertEquals(expected, result);

    }
}