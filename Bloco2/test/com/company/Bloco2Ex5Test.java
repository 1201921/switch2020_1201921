package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex5Test {

    @Test
    void getVolumeDM3() {
        double area = 864;

        double expected = 1.728;
        double result = Bloco2Ex5.getVolumeDM3(area);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void getCubeSizeMedio() {
        double area = 864;
        String expected = "O cubo é médio";
        String result = Bloco2Ex5.getCubeSize(area);
        assertEquals(expected, result);
    }

    @Test
    void getCubeSizeGrande() {
        double area = 1000;
        String expected = "O cubo é grande";
        String result = Bloco2Ex5.getCubeSize(area);
        assertEquals(expected, result);
    }

    @Test
    void getCubeSizePequeno() {
        double area = 86;
        String expected = "O cubo é pequeno";
        String result = Bloco2Ex5.getCubeSize(area);
        assertEquals(expected, result);
    }
}