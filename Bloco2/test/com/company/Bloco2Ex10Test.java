package com.company;

import com.company.Bloco2Ex10;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex10Test {

    @Test
    void precoDesconto60porcento() {
        double precoOriginal = 268;

        double expected = 107.2;
        double result = Bloco2Ex10.precoDesconto(precoOriginal);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void precoDesconto40porcento() {
        double precoOriginal = 188;

        double expected = 112.8;
        double result = Bloco2Ex10.precoDesconto(precoOriginal);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void precoDesconto30porcento() {
        double precoOriginal = 88;

        double expected = 61.6;
        double result = Bloco2Ex10.precoDesconto(precoOriginal);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void precoDesconto20porcento() {
        double precoOriginal = 40;

        double expected = 32;
        double result = Bloco2Ex10.precoDesconto(precoOriginal);
        assertEquals(expected, result, 0.01);
    }
}