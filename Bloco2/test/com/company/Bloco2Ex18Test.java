package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex18Test {

    @Test
    void horaFimTeste1() {
        int hI=4, mI=40, sI=20, tP=3600;

        String expected = "O fim do processamento dá-se às 5:40:20";
        String result = Bloco2Ex18.horaFim(hI, mI, sI, tP);
        assertEquals(expected, result);
    }

    @Test
    void horaFimTeste2() {
        int hI=16, mI=15, sI=25, tP=8957;

        String expected = "O fim do processamento dá-se às 18:44:42";
        String result = Bloco2Ex18.horaFim(hI, mI, sI, tP);
        assertEquals(expected, result);
    }
}