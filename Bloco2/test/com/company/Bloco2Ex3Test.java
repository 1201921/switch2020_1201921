package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex3Test {

    @Test
    void distPontos() {
        int x1=-3, x2=4, y1=5, y2=-7;
        double expected=13.892;
        double result = Bloco2Ex3.distPontos(x1,x2,y1,y2);
        assertEquals(expected, result, 0.01);
    }
}