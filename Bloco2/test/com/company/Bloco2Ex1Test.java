package com.company;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex1Test {

    @org.junit.jupiter.api.Test
    void mediaPesada() {
        double nota1=15, nota2=17, nota3=17;
        double peso1=30, peso2=30, peso3=40;

        double expected=16.4;
        double result = Bloco2Ex1.mediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);
        assertEquals(expected, result, 0.01);

    }
}