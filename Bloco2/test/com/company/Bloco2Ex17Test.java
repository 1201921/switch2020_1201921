package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex17Test {


    @Test
    void horaChegadaHoje() {
        int hP=8, mP=56, hD=1, mD=40;

        String expected = "O comboio chega às: 10:36h do dia de hoje";
        String result = Bloco2Ex17.horaChegada(hP,mP,hD,mD);
        assertEquals(expected, result);
    }

    @Test
    void horaChegadaAmanha() {
        int hP=22, mP=30, hD=4, mD=30;

        String expected = "O comboio chega às: 3:0h do dia de amanhã";
        String result = Bloco2Ex17.horaChegada(hP,mP,hD,mD);
        assertEquals(expected, result);
    }
}