package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco2Ex11Test {

    @Test
    void avaliacaoTurmaTeste1() {
        double aprovados=1.1;

        String expected = "Valor inválido";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void avaliacaoTurmaTeste2() {
        double aprovados=1;

        String expected = "Turma excelente";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void avaliacaoTurmaTeste3() {
        double aprovados=0.8;

        String expected = "Turma boa";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void avaliacaoTurmaTeste4() {
        double aprovados=0.65;

        String expected = "Turma razoável";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void avaliacaoTurmaTeste5() {
        double aprovados=0.3;

        String expected = "Turma fraca";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void avaliacaoTurmaTeste6() {
        double aprovados=0.14;

        String expected = "Turma má";
        String result = Bloco2Ex11.avaliacaoTurma(aprovados);
        assertEquals(expected, result);
    }
}