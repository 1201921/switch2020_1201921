package com.company;

import java.util.Scanner;
public class Bloco2Ex10 {


    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        double precoOriginal=1;
        System.out.println("Insira o preço original do artigo: ");
        if (ler.hasNextDouble() == true) precoOriginal = ler.nextDouble();

        double precoDesconto = precoDesconto(precoOriginal);

        System.out.println(precoDesconto);
    }

    public static double precoDesconto (double precoOriginal){
        double precoDesconto;
        if (precoOriginal>200) return precoDesconto = precoOriginal*0.4;
        else if ((precoOriginal<=200) && (precoOriginal>100)) return precoDesconto = precoOriginal*0.6;
        else if ((precoOriginal<=100) && (precoOriginal>=50)) return precoDesconto = precoOriginal*0.7;
        else return precoDesconto = precoOriginal*0.8;
    }
}
