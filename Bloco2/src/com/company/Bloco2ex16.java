package com.company;
import java.util.Scanner;
public class Bloco2ex16 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        double a,b,c;
        System.out.println("Insira o angulo de a:");
        a= ler.nextDouble();
        System.out.println("Insira o angulo de b:");
        b= ler.nextDouble();
        System.out.println("Insira o angulo de c:");
        c= ler.nextDouble();

    }

    public static String tipoTriangulo (double a, double b, double c){
        if ((a<=0) || (b<=0) || (c<=0)) return "Nenhum angulo pode ser inferior a 0º";
        else if (a + b + c != 180) return "A soma dos angulos internos de triangulo tem de ser igual a 180º";
        else{
            if ((a==90) ^ (b==90) ^ (c==90)) return "Triangulo Rectangulo";
            if ((a<90) && (b<90) && (c<90)) return "Triangulo Acutangulo";
            else return "Triangulo Obtusangulo";
        }
    }
}
