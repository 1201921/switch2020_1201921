package com.company;

public class Bloco2Ex20 {
    public static void main(String[] args) {
        int diaSemana, tipoKit, kms;



    }

    public static String preco (int diaSemana, int tipoKit, double kms){
        double precoTransporte=kms*2;

        if (diaSemana==1 || diaSemana==2 || diaSemana==3 || diaSemana==4 || diaSemana==5) {
            if (tipoKit==1) return "O preço do aluguer é de 30€ e " + precoTransporte + "€ de transporte";
            else if (tipoKit==2) return "O preço do aluguer é de 50€ e " + precoTransporte + "€ de transporte";
            else if (tipoKit==3) return "O preço do aluguer é de 100€ e " + precoTransporte + "€ de transporte";
            else return "Tipo de Kit inválido";
        }

        if (diaSemana==6 || diaSemana==7 || diaSemana==8){
            if (tipoKit==1) return "O preço do aluguer é de 40€ e " + precoTransporte + "€ de transporte";
            else if (tipoKit==2) return "O preço do aluguer é de 70€ e " + precoTransporte + "€ de transporte";
            else if (tipoKit==3) return "O preço do aluguer é de 140€ e " + precoTransporte + "€ de transporte";
            else return "Tipo de Kit inválido";
        }

        else return "Dia da semana inválido";
    }

}
