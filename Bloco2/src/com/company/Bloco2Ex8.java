package com.company;
import java.util.Scanner;
public class Bloco2Ex8 {
    public static void main(String[] args) {
        Scanner Ler = new Scanner(System.in);
        int x, y;
        System.out.println("Insira o valor de x:");
        x=Ler.nextInt();
        System.out.println("Insira o valor de y:");
        y=Ler.nextInt();

        System.out.println(confirmarMultiplo(x,y));
    }

    public static String confirmarMultiplo (int x, int y){
        if (y%x==0) return "y é multiplo de x";
        else if (x%y==0) return "x é multiplo de y";
        else return "x não é multiplo nem dividor de y";
    }
}
