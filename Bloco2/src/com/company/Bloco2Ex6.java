package com.company;
import java.util.Scanner;
public class Bloco2Ex6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int momentoDia=0;

        System.out.println("Insira os segundos do momento do dia:");
        if(input.hasNextInt() == true) momentoDia = input.nextInt();   //3665

        System.out.println(getHoras(momentoDia) + ":" + getMinutos(momentoDia) + ":" + getSegundos(momentoDia));

        if ((momentoDia>=21600) && (momentoDia<=43200)) System.out.println("Bom dia");
        else if ((momentoDia>43200) && ((momentoDia)<=72000)) System.out.println ("Bom tarde");
        else  System.out.println("Boa noite");
    }

    public static int getSegundos(int momentoDia) {
        int segundos = (int) momentoDia%60;
        return segundos;
    }

    public static int getMinutos(int momentoDia){
        int minutos = (int) momentoDia % 3600/60;
        return minutos;
    }

    public static int getHoras (int momentoDia){
        int horas = (int) momentoDia/3600;
        return horas;
    }
}
