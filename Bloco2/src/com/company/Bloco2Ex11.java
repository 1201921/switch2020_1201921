package com.company;

import java.util.Scanner;
public class Bloco2Ex11 {
    public static void main (String [] args){

        /* a) O utilizador insere a percentagem de alunos aprovados (em numero decimal
        e 6 cenários podem resultar: 1) Inválido caso a percentagem seja <0 ou >1;
        2) Turma má no caso da percentagem se situar no intervalo [0-0.2[; 3) Turma fraca
        no intervalo [0.2-0.5[; 4) Turma razoável  no intervalo [0.5-0.7[; 5) Turma boa
        no intervalo [0.7-0.9[; e 6) Turma excelente no intervalo [0.9-1]
        */

        Scanner ler = new Scanner(System.in);
        double aprovados=0; // % em decimal
        System.out.println("Indique a % de alunos aprovados: ");
        if (ler.hasNextDouble() == true) aprovados = ler.nextDouble();

        System.out.println(avaliacaoTurma(aprovados));


    }

    public static String avaliacaoTurma (double aprovados) {

        if ((aprovados<0) || (aprovados>1)) return "Valor inválido";
        else if ((aprovados>=0) && (aprovados<0.2)) return "Turma má";
        else if ((aprovados>=0.2) && (aprovados<0.5)) return "Turma fraca";
        else if ((aprovados>=0.5) && (aprovados<0.7)) return "Turma razoável";
        else if ((aprovados>=0.7) && (aprovados<0.9)) return "Turma boa";
        else return "Turma excelente";
    }
}
    /*
    e) acrescentar pergunta ao utilizador para cada um dos valores que determina os limites dos intervalos
     double a=0, b=0, c=0, d=0;
     System.out.prinln ("Qual o limite máximo do intervalo correspondente a "Turma má?");
     if (ler.hasNextDouble() == true) a = ler.nextDouble();

     System.out.prinln ("Qual o limite máximo do intervalo correspondente a "Turma fraca?");
     if (ler.hasNextDouble() == true) b = ler.nextDouble();

     (...)

     if ((aprovados<0) || (aprovados>1)) return "Valor inválido";
        else if ((aprovados>=0) && (aprovados<a)) return "Turma má";
        else if ((aprovados>=a) && (aprovados<b)) return "Turma fraca";
        else if ((aprovados>=b) && (aprovados<c)) return "Turma razoável";
        else if ((aprovados>=c) && (aprovados<d)) return "Turma boa";
        else return "Turma excelente";
     */