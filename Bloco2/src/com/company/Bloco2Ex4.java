package com.company;

public class Bloco2Ex4 {
    public static void main(String[] args) {
        int x = 4;
        if (x<0) System.out.println("x = " + xNegativo(x));
        if (x==0) System.out.println ("x = " + xZero(x));
        if (x>0) System.out.println("x = " + xPositivo(x));


    }

    public static int xPositivo(int x){
        int valorX = (int)(Math.pow(x,2)-(2*x));
        return valorX;
    }

    public static int xNegativo (int x){
        int valorX = x;
        return valorX;
    }

    public static int xZero (int x){
        int valorX = 0;
        return valorX;
    }
}
