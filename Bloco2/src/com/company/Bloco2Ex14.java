package com.company;
import java.util.Scanner;
public class Bloco2Ex14 {
    public static void main(String[] args) { //1Milha = 1609mts
        Scanner ler = new Scanner (System.in);
        double dia1=0, dia2=0, dia3=0, dia4=0, dia5=0;
        System.out.println("indique a média de milhas do 1º dia: ");
        if (ler.hasNextDouble() == true) dia1 = ler.nextDouble();
        System.out.println("indique a média de milhas do 2º dia: ");
        if (ler.hasNextDouble() == true) dia2 = ler.nextDouble();
        System.out.println("indique a média de milhas do 3º dia: ");
        if (ler.hasNextDouble() == true) dia3 = ler.nextDouble();
        System.out.println("indique a média de milhas do 4º dia: ");
        if (ler.hasNextDouble() == true) dia4 = ler.nextDouble();
        System.out.println("indique a média de milhas do 5º dia: ");
        if (ler.hasNextDouble() == true) dia5 = ler.nextDouble();

        System.out.println(getMedia (dia1, dia2, dia3, dia4, dia5));
    }
    public static double getMedia (double dia1, double dia2, double dia3, double dia4, double dia5){
        double mediaKMS = ((dia1 + dia2 + dia3 + dia4 + dia5) / 5) * 1.609;
        return mediaKMS;
    }
}
