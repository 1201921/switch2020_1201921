package com.company;

public class Bloco2Ex3 {
    public static void main(String[] args) {
        int x1=-3, x2=4, y1=5, y2=-7;

        System.out.println(distPontos(x1,x2,y1,y2));

    }

    public static double distPontos (int x1, int x2, int y1, int y2){
        double distancia = Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));
        return distancia;
    }
}
