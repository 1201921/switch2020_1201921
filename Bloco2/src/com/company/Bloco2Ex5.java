package com.company;

public class Bloco2Ex5 {
    public static void main(String[] args) {
        /* double area, aresta=44;
        double volumeDM3;
        area= Math.pow(aresta, 2);
        aresta =  Math.sqrt(area);
        volumeDM3 =  Math.pow (aresta, 3);
        //volumeDM3 = Math.pow((Math.cbrt(2)),3); para dar volume médio

        if ((volumeDM3)<=1) System.out.println("O cubo é pequeno");
        else if ((volumeDM3)>2) System.out.println("O cubo é grande");
        else System.out.println("o cubo é médio");

        System.out.println ("volume: " + volumeDM3);


        //System.out.println("aresta: " + aresta + "cm" + "\narea: " + area + "cm2" + "\nvolume: " + volumeDM3 + "dm3");

         */

        double area = 864;

        System.out.println("aresta: " + getAresta(area) + "cm" + "\narea: " + area + "cm2" + "\nvolume: " + getVolumeDM3(area) + "dm3");
        System.out.println(getCubeSize(area));

    }

    public static double getAresta(double area) {
        double aresta = Math.sqrt(area / 6);
        return aresta;
    }

    public static double getVolumeDM3(double area) {
        double volumeDM3 = (Math.pow(getAresta(area), 3) / 1000); // tem de /1000 pq a medida da aresta está em cm.
        return volumeDM3;
    }

    public static String getCubeSize(double area) {
        if ((getVolumeDM3(area)) <= 1) return "O cubo é pequeno";
        else if ((getVolumeDM3(area)) > 2) return "O cubo é grande";
        else return "O cubo é médio";
    }
}

