package com.company;

public class Bloco2Ex7 {
    public static void main(String[] args) {
        double areaEdificio, custoLitroTinta, rendTrabDia, areaLitro, salarioDia;

        areaEdificio = 1000; //tem de ser perguntado
        custoLitroTinta = 5; //tem de ser perguntado
        areaLitro= 0.8; //ex: cada litro de tinta dá para pintar 0.8m2; tem de ser perguntado
        salarioDia=30; //ex: 30€/dia; tem de ser perguntado
        rendTrabDia=16; //2m2/h * 8h/dia = 16m2/dia; valores dados no enunciado


        double numPintores = getNumPintores(areaEdificio);
        double tintaUtilizada = getTintaGasta(areaEdificio,areaLitro);
        double custoTintaGasta = tintaUtilizada * custoLitroTinta;
        double diasTrabalho = areaEdificio/rendTrabDia/getNumPintores((areaEdificio));
        double custoMaoObra = numPintores * salarioDia * diasTrabalho;
        double custoTotal = custoTintaGasta + custoMaoObra;

        System.out.println("custo total: " + custoTotal + "€.");
        System.out.println("Custo mão de Obra: " + custoMaoObra + "€.");
        System.out.println("Custo da Tinta: " + custoTintaGasta + "€.");

    }

    public static double getNumPintores (double areaEdificio){
        int numPintores;
        if (areaEdificio>=1000) return 4;
        else if ((areaEdificio>=300) && (areaEdificio<1000)) return 3;
        else if ((areaEdificio>=100) && (areaEdificio<300)) return 2;
        else return 1;
    }

    public static double getTintaGasta (double areaEdificio, double areaLitro){
        double tintaUtilizada;
        tintaUtilizada = areaEdificio/areaLitro;
        return tintaUtilizada;
    }

    public static double getCustoMaoObra (double numPintores, double salarioDia, double diasTrabalho){
        double custoMaoObra = numPintores*salarioDia*diasTrabalho;
        return custoMaoObra;
    }


}

