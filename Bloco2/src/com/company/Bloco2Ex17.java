package com.company;

public class Bloco2Ex17 {
    public static void main(String[] args) {

        int hP, mP, hD, mD; //horaPartida, minutoPartida, horaDuração, minutoDuração

    }

    public static String horaChegada (int hP, int mP, int hD, int mD) {
        int momentoChegada = hP * 60 + mP + hD * 60 + mD; //momento de chegada em minutos
        int hC = momentoChegada / 60;
        int mC = momentoChegada % 60;

        if (hC >= 24) {
            hC = hC - 24;
            return ("O comboio chega às: " + hC + ":" + mC + "h do dia de amanhã");
        }
        else return ("O comboio chega às: " + hC + ":" + mC + "h do dia de hoje");
            //Uma alternativa seria colocar String dia=hoje logo após os dados e ele ia assumir por defeito que o dia era hoje.
            // Se entrasse no if então teriamos o dia amanhã. Nesse caso, só seria necessário um return e não era necessário o else.
    }
}