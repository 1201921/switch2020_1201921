package com.company;
import java.util.Scanner;
public class Bloco2Ex15 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        //a) e b) Dizer se o triangulo é possível (nº>0 e cada lado ser menor que a soma dos outros dois
        double lado1, lado2, lado3;
        System.out.println("insira o comprimento do lado 1 do triangulo");
        lado1=ler.nextDouble();
        System.out.println("insira o comprimento do lado 2 do triangulo");
        lado2=ler.nextDouble();
        System.out.println("insira o comprimento do lado 3 do triangulo");
        lado3=ler.nextDouble();

        System.out.println(tipoTriangulo(lado1, lado2, lado3));
    }

    public static String tipoTriangulo (double lado1, double lado2, double lado3){
        if ((lado1<=0) || (lado2<=0) || (lado3<=0)) return "Medidas inválidas";
        else if ((lado1>lado2+lado3) || (lado2>lado1+lado3) || (lado3 > lado1+lado2)) return "Triangulo impossível";
        else {
            if ((lado1 == lado2) && (lado2 == lado3)) return "Triangulo Equilátero";
            else if ((lado1 == lado2) ^ (lado2 == lado3) ^ (lado3 == lado1)) return "Triangulo Isósceles";
            else return "Triangulo Escaleno";
        }
    }
}
