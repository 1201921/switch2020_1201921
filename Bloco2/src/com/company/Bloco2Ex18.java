package com.company;

public class Bloco2Ex18 {
    public static void main(String[] args) {
        int hI, mI, sI, tP; //horaInicio, minutoInicio, segundoInicio, tempoProcessamento

    }

    public static String horaFim (int hI, int mI, int sI, int tP){
        int momentoFinal = hI*3600 + mI*60 + sI + tP;
        int hP = momentoFinal/3600; //horaProcessamento
        int mP = (momentoFinal%3600)/60; //minutoProcessamento
        int sP = momentoFinal%60;

        return ("O fim do processamento dá-se às " + hP + ":"
                + mP + ":" + sP);
    }
}
