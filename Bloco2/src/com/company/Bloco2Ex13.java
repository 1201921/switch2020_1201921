package com.company;
import java.util.Scanner;
public class Bloco2Ex13 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        double areaGrama;
        int numArvores, numArbustos;
        System.out.println("area a plantar de Grama:");
        areaGrama = ler.nextDouble();
        System.out.println("Nº arvores:");
        numArvores = ler.nextInt();
        System.out.println("NºArbustos:");
        numArbustos = ler.nextInt();

        int maoObra = 10;//h
        double tempoGrama = 300.0/3600, tempoArvore = 600.0/3600, tempoArbusto = 400.0/3600;
        int custoArvore = 20, custoArbusto = 15, custoGrama=10;

        double custoTotal = custoTotal (maoObra, tempoGrama, tempoArvore, tempoArbusto, custoArvore, custoArbusto,
                custoGrama, numArvores, numArbustos, areaGrama);
        double horasServiço = horasServiço(numArvores,numArbustos, areaGrama, tempoArbusto, tempoArvore, tempoGrama);

        System.out.println("O custo total do serviço é: " + custoTotal);
        System.out.println("O nº total de horas de serviço é: " + horasServiço);
    }

    public static double custoTotal (int maoObra, double tempoGrama, double tempoArvore, double tempoArbusto,
                                     int custoArvore, int custoArbusto, int custoGrama, int numArvores, int
                                             numArbustos, double areaGrama){
        double custoTotal = (custoGrama*areaGrama + maoObra*areaGrama*tempoGrama) +
                (custoArbusto*numArbustos + maoObra*numArbustos*tempoArbusto) +
                (custoArvore*numArvores + maoObra*numArvores*tempoArvore);
        return custoTotal;
    }

    public static double horasServiço (int numArvores, int numArbustos, double areaGrama, double
                                       tempoArbusto, double tempoArvore, double tempoGrama){
        double horasServiço = numArvores*tempoArvore + numArbustos*tempoArbusto + areaGrama*tempoGrama;
        return horasServiço;
    }

}
