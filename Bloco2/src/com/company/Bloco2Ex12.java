package com.company;
import java.util.Scanner;
public class Bloco2Ex12 {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        double indPoluicao = 0;
        System.out.println("Insira o Indice de Poluição dos 3 grupos de indústrias: ");
        if (ler.hasNextDouble() == true) indPoluicao = ler.nextDouble();

        System.out.println(notificação(indPoluicao));

    }

    public static String notificação (double indPoluicao){
        if (indPoluicao>0.3) {
            if ((indPoluicao > 0.3) && (indPoluicao <= 0.4))
                return "As industrias do 1º grupo têm de suspender atividades";
            else if ((indPoluicao > 0.4) && (indPoluicao <= 0.5))
                return "As industrias do 1º e 2º grupos têm de suspender atividades";
            else return "Todas as industrias têm de suspender atividades";
        }
        else return "Podem continuar as atividades";
    }
}
