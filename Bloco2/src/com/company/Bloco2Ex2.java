package com.company;

public class Bloco2Ex2 {
    public static void main(String[] args) {

        //Resoluçao até ao ponto d)
        /*int numero = 627;
        int digito3 = numero % 10;
        int digito2 = (numero / 10) % 10;
        int digito1 = (numero / 100) % 10;


        if ((numero < 100) || (numero > 999)) System.out.println("Número não tem 3 dígitos");
        else{
            System.out.println("Número: " + numero);
            if (digito1 % 2 == 0) System.out.println("1º digito: "+ digito1 + " é nº par");
            else System.out.println("1º digito: "+ digito1 + " é nº impar");

            if (digito2 % 2 == 0) System.out.println("2º digito: " + digito2 + " é nº par");
            else System.out.println("2º digito: " + digito2 + " é nº impar");

            if (digito3 % 2 == 0) System.out.println("3º digito: " + digito3 + " é nº par");
            else System.out.println("3º digito: " + digito3 + " é nº impar");
        }*/

        //Resolução a partir do ponto e)
        int numero=658;
        System.out.println("1º digito: " + getDigito1(numero));
        System.out.println("2º digito: " + getDigito2(numero));
        System.out.println("3º digito: " + getDigito3(numero));
    }
        public static int getDigito1 (int numero){
            int digito1 = (numero / 100) % 10;
            return digito1;
        }

        public static int getDigito2 (int numero){
            int digito2 = (numero / 10) % 10;
            return digito2;
        }

        public static int getDigito3 (int numero){
            int digito3 = numero % 10;
            return digito3;
        }
}
